import numpy as np
import matplotlib.pyplot as plt
import tensorflow as tf
import cv2

from tensorflow.keras import datasets, layers, models

from common.params import ALL_CHANNELS, DEMO_CHANNELS, DEMO_S, SAMPLE_RATE, RQA_PARAMS

def main():
    plt.rcParams['figure.constrained_layout.use'] = True
    plt.close('all')
    Ss = 109 + 1

    data = np.zeros(((Ss-1)*48, 64, 64, 4), dtype=np.uint8)
    test_data = np.zeros(((Ss-1)*48, 64, 64, 4), dtype=np.uint8)
    labels = np.zeros(((Ss-1)*48,), dtype=np.uint8)

    for s in range(1, Ss):
        for r in (1, 2):
            for kk, channel in enumerate(DEMO_CHANNELS):
                print(f"crp/S{s:03d}R{r:02d}/{channel}_0.png")
                d1 = cv2.imread(f"crp/S{s:03d}R{r:02d}/{channel}_0.png", cv2.IMREAD_GRAYSCALE) / 255
                print(f"crp/S{s:03d}R{r:02d}/{channel}_800.png")
                d2 = cv2.imread(f"crp/S{s:03d}R{r:02d}/{channel}_800.png", cv2.IMREAD_GRAYSCALE) / 255
                for i in range(24):
                    count  = (s-1)*48+i*2+(r-1)
                    labels[count] = r-1
                    data[count, :, :, kk] = d1[-(i+1)*64-1:-i*64-1,i*64:(i+1)*64]
                    test_data[count, :, :, kk] = d2[-(i+1)*64-1:-i*64-1,i*64:(i+1)*64]

    np.save("cnn_train_data", data)
    np.save("cnn_test_data", test_data)
    np.save("cnn_labels", labels)


if __name__ == "__main__":
    main()