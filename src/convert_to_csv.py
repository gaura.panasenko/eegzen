#!/usr/bin/env python3
import json

cols = ["tau","m","L_min","V_min","W_min","RR","DET","L","L_max","DIV","L_entr","LAM","TT","V_max","V_entr","W","W_max","W_div","W_entr","DET/RR","LAM/DET"]

def main():
    if input("Are you sure? (y/n)") != "y":
        return
    with open('rqa.json', 'r', encoding='utf-8') as f:
        data = json.load(f)
    data_out = "," + ",".join(cols) + "\n"
    for k,v in data.items():
        dt = ",".join([str(v[i]) for i in cols])
        data_out += f"{k},{dt}\n"
    with open('rqa.csv', 'w', encoding='utf-8') as f:
        f.write(data_out)

if __name__ == "__main__":
    main()