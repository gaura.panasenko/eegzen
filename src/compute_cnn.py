import numpy as np
import matplotlib.pyplot as plt
import tensorflow as tf

from tensorflow.keras import datasets, layers, models

from common.params import ALL_CHANNELS, DEMO_CHANNELS, DEMO_S, SAMPLE_RATE, RQA_PARAMS

def main():
    plt.rcParams['figure.constrained_layout.use'] = True
    plt.close('all')
    Ss = 109 + 1

    data = np.zeros((Ss-1, 2, 4, 10, 24))

    out = {}

    for s in range(1, Ss):
        for r in (1, 2):
            data[s-1, r-1 , :, :, :23] = np.load(f"rqa/S/S{s:03d}R{r:02d}.npy")
    alphas = np.load(f"alphas.npy")
    data[:,:,:,:,23] = alphas[:,:,:,::5]

    for kk, channel in enumerate(DEMO_CHANNELS):
        for i in range(24):
            dt = data[:,:,kk,:,i]
            min_i = dt.min()
            max_i = dt.max()
            scale = max_i - min_i
            if scale == 0:
                scale = 1
            dt -= min_i
            dt /= scale

    data1 = data.mean(axis=3)

    model = models.Sequential()
    model.add(layers.Conv2D(128, (3, 3), activation='relu', input_shape=(4, 10, 24)))
    model.add(layers.MaxPooling2D((1, 2)))
    model.add(layers.Conv2D(256, (1, 3), activation='relu'))
    model.add(layers.Flatten())
    model.add(layers.Dense(256, activation='relu'))
    model.add(layers.Dense(40))
    model.summary()
    model.compile(optimizer='adam',
              loss=tf.keras.losses.SparseCategoricalCrossentropy(from_logits=True),
              metrics=['accuracy'])
    
    train_images = np.concatenate((data[:,0],data[:,1]), axis=0)
    train_labels = np.array((0,) * 109 + (1,)*109)
    # train_images=train_images.reshape((218, -1),order="F")
    print(train_images.shape)


    history = model.fit(train_images, train_labels, epochs=20, 
                    validation_data=(train_images, train_labels))
    plt.plot(history.history['accuracy'], label='accuracy')
    plt.plot(history.history['val_accuracy'], label='val_accuracy')
    print(history.history['accuracy'][-1] * 100)
    plt.xlabel('Epoch')
    plt.ylabel('Accuracy')
    plt.ylim([0.5, 1])
    plt.legend(loc='lower right')




if __name__ == "__main__":
    main()