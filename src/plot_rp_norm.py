import matplotlib.pyplot as plt
import matplotlib.style as mplstyle
import numpy as np
import json
import math
from scipy import signal
from numpy import linalg as LA

from common.params import ALL_CHANNELS, DEMO_CHANNELS, DEMO_S, SAMPLE_RATE, RQA_PARAMS, EPS
from common.rqa_analyse import calc_rp, calc_rqa
from teaspoon.parameter_selection.FNN_n import FNN_n
from teaspoon.parameter_selection.MI_delay import MI_for_delay

from common import filter_noise_stft

FAST = False

def main():
    if FAST:
        mplstyle.use('fast')
    plt.rcParams['figure.constrained_layout.use'] = True
    # plt.rcParams['text.usetex'] = True
    plt.close('all')
    
    segment = int(SAMPLE_RATE * 5)
    w = f"{segment / SAMPLE_RATE:0.2f}"
    channel_name = DEMO_CHANNELS[2]
    channel = ALL_CHANNELS[channel_name] 
    eps = 1.5
    tau = 1
    m = 4

    for s in range(1, 14):
        s = f"S{s:03d}"

        r_names = ["Opened eyes (R01)", "Closed eyes (R02)"]
        
        #fig1, ax1 = plt.subplots(2, 1, sharex=True, sharey=True)
        #fig1.suptitle(f"Normalized signal for {s}")
        fig, ax = plt.subplots(2, 2, sharex=True, sharey="row", height_ratios=[0.2,0.8], figsize=(7, 4), dpi=160)
        fig.suptitle(f"Reccurence plot for {s} {channel_name}, $\\epsilon$={eps}, $m$={m}, $\\tau$={tau}")
        
        for r in range(1, 3):
            file = f"data/{s}R{r:02d}.npy"
            print(file)
            data = np.load(file)[:, channel]
            x = filter_noise_stft(data, SAMPLE_RATE, segment)
            start = segment * 10
            end = start + segment
            x = x[start:end]
            x -= x.mean()
            #x = np.tanh(x/np.max(np.abs(x))*2)
            x_mean = x.mean()
            x_std = np.std(x)
            x = (x - x_mean) / x_std
            x_t = np.arange(start, end) / SAMPLE_RATE
            ax[0, r-1].plot(x_t, x)
            ax[0, r-1].set_title(r_names[r-1])
            #ax[0, r-1].set_xlabel("Time, s")
            bound = max(np.abs(ax[0, r-1].get_ylim()))
            ax[0, r-1].set_ylim(-bound, bound)
            rp = calc_rp(x, tau, m, 0, 1, segment, eps, None)
            rqa = calc_rqa(x, tau, m, eps)
            rqa_str = [f"{key}={value:0.3f}" for key, value in rqa.items() if not "min" in key]
            rqa_str = "\n".join(rqa_str)
            start /= SAMPLE_RATE
            end /= SAMPLE_RATE
            ax[1, r-1].imshow(rp, cmap="Grays", interpolation='none' if FAST else 'auto', extent=[start, end] * 2)
            #ax[1, r-1].set_title(r_names[r-1])
            ax[1, r-1].set_xlabel("Time, s")
            ax[1, r-1].text(1.05, 1, rqa_str, horizontalalignment='left', verticalalignment='top', transform=ax[1, r-1].transAxes, size="small")

    plt.show()


if __name__ == "__main__":
    main()
