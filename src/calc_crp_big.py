#!/usr/bin/env python3
import os
import numpy as np
from numpy.fft import rfft, irfft, rfftfreq
import cv2


from common.rqa_analyse import calc_rp
from common.params import ALL_CHANNELS, DEMO_CHANNELS, SAMPLE_RATE
from common import filter_noise_min

# ["Af7", "Af8", "Tp7", "Tp8", "O1", "O2", "Fp1", "Fp2"]

nseg = int(SAMPLE_RATE * 5) * 2
f = rfftfreq(nseg, 1 / SAMPLE_RATE)
noise_free = filter_noise_min(f)
tau, m = 4, 3

def norm(x):
    return np.sqrt(np.mean(x**2)) * 2

def calc(name, filename):
    x = np.load(filename)
    nsegs = (x.shape[0] - nseg) // (nseg // 2)

    for c, channel in enumerate(DEMO_CHANNELS):
        e = ALL_CHANNELS[channel]
        if not os.path.isdir(f"crp/{name}"):
            os.mkdir(f"crp/{name}")

        for i in range(nsegs):
            out_file = f"crp/{name}/{channel}_{i*nseg // 2}.png"
            if os.path.exists(out_file):
                continue
            x_i = x[i*nseg//2:i*nseg//2+nseg, e]
            x_i = irfft(rfft(x_i) * noise_free)
            xx, yy = np.meshgrid(x_i, x_i)
            eps = np.abs(xx - yy).mean()
            mat = calc_rp(x_i, tau, m, 0, eps=eps)
            mat = 255 - mat * 255
            cv2.imwrite(out_file, mat.astype('uint8'))

def main():
    if input("Are you sure? (y/n)") != "y":
        return
    for i in range(1, 110):
        i_name = f"S{i:03d}"
        # for j in range(3, 5):
        #     j_name = f"R{j:02d}"
        #     for k in range(3):
        #         name = f"{i_name}{j_name}T{k}"
        #         print(f"Processing {name}...")
        #         filename = f"data/T/{name}.npy"
        #         calc(name, filename, data)
        for j in range(1, 3):
            j_name = f"R{j:02d}"
            name = f"{i_name}{j_name}"
            print(f"Processing {name}...")
            filename = f"data/{name}.npy"
            calc(name, filename)

if __name__ == "__main__":
    main()