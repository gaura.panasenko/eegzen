import pyedflib
import numpy as np

name = "S001R01"


def convert_edf(source, target, timed=False):
    f = pyedflib.EdfReader(f"{source}.edf")
    try:

        signal_labels = f.getSignalLabels()
        ids = [-1, -1, -1, -1, -1]
        for i in range(len(signal_labels)):
            for jk, jv in enumerate(["Fp1", "Fp2", "Af7", "O1", "O2"]):
                if signal_labels[i].startswith(jv):
                    ids[jk] = i
                    # print(jv, i)

        freq = f.getSampleFrequency(0)

        ranges_dict = {}

        ann = f.readAnnotations()
        max_len = int(max(ann[1] * freq))
        starts = (ann[0] * freq).astype(np.int64)

        if timed:
            ranges = np.array([range(i, i + max_len + 1) for i in starts], dtype=np.int64)
            for i in set(ann[2]):
                ranges_dict[i] = ranges[ann[2] == i].flatten()

            min_len = min([i.shape[0] for i in ranges_dict.values()])
            for k, v in ranges_dict.items():
                ranges_dict[k] = v[v < min_len]
            for rk, rv in ranges_dict.items():
                sigbufs = np.zeros((len(rv), len(signal_labels))) # f.getNSamples()[0]
                for i, _ in enumerate(signal_labels):
                    sigbufs[:, i] = f.readSignal(i)[rv]
                np.save(f"{target}{rk}", sigbufs)
        else:
            n = f.signals_in_file
            sigbufs = np.zeros((f.getNSamples()[0], len(signal_labels)))
            for i, _ in enumerate(signal_labels):
                sigbufs[:, i] = f.readSignal(i)
            np.save(f"{target}", sigbufs)
    finally:
        f.close()

    #np.save(f"{target}", sigbufs)

def main():
    #convert_edf(name, name)
    if input("Are you sure? (y/n)") != "y":
        return
    for i in range(1, 110):
        i_name = f"S{i:03d}"
        for j in range(3, 5):
            j_name = f"R{j:02d}"
            print(f"data/{i_name}/{i_name}{j_name}", f"data/{i_name}{j_name}")
            convert_edf(f"data/{i_name}/{i_name}{j_name}", f"data/T/{i_name}{j_name}", True)
        # for j in range(1, 5):
        #     j_name = f"R{j:02d}"
        #     print(f"data/{i_name}/{i_name}{j_name}", f"data/{i_name}{j_name}")
        #     convert_edf(f"data/{i_name}/{i_name}{j_name}", f"data/{i_name}{j_name}")

if __name__ == "__main__":
    main()
