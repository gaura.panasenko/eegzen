import matplotlib.pyplot as plt
import numpy as np

from common.params import RQA_ORDER, DEMO_CHANNELS, RQA_AVGS

plot_filt = (2,) + tuple(range(7, 18)) + tuple(range(19, 23))

def main():
    plt.rcParams['figure.constrained_layout.use'] = True
    plt.rcParams['text.usetex'] = True
    plt.close('all')
    for s in range(1, 110):
        for e in range(4):
            channel = DEMO_CHANNELS[e]
            x = np.load(f"rqa/S/S{s:03d}R01.npy")
            y = np.load(f"rqa/S/S{s:03d}R02.npy")
            t = np.arange(x.shape[1]) * 5
            fig, ax = plt.subplots(4, 4, sharex=True, label=f"S{s:03d} - {channel}")
            fig.suptitle(f"S{s:03d} - {channel}")
            for k_i, v_i in enumerate(plot_filt):
                x_i = x[e,:,v_i]
                y_i = y[e,:,v_i]
                ax_i = ax[k_i//4,k_i%4]
                kwargs = {}
                if k_i == 3:
                    kwargs["label"] = "Closed eyes $R_1$"
                ax_i.plot(t, x_i, **kwargs)
                if k_i == 3:
                    kwargs["label"] = "Opened eyes $R_2$"
                ax_i.plot(t, y_i, **kwargs)
                if k_i == 3:
                    ax_i.legend()
                ax_i.set_title(f"{RQA_ORDER[v_i]} $R_1$={x_i.mean():0.2f} $R_2$={y_i.mean():0.2f}")
                x_max = max(x_i.max(), y_i.max())
                x_min = min(x_i.min(), y_i.min())
                x_min, x_max = x_min - (x_max - x_min) * 0.2, x_max + (x_max - x_min) * 0.2
                if RQA_ORDER[v_i] in RQA_AVGS:
                    mm = RQA_AVGS[RQA_ORDER[v_i]][e]
                    if x_min < mm and mm < x_max:
                        ax_i.axhline(y=mm, color='0.6', linestyle='--')
                ax_i.set_xlabel("Time, s")
            fig.set_size_inches(30//2, 18//2)
            fig.savefig(f"figs/RQA_S{s:03d}_{channel}.png")
            plt.close()

if __name__ == "__main__":
    main()
