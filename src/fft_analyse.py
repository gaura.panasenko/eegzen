#!/usr/bin/env python3
from scipy.signal import check_COLA
import numpy as np
import matplotlib.pyplot as plt
from sklearn.decomposition import FastICA

from common.params import ALL_CHANNELS
from common.process_eeg import ProcessEEG

FILE = "data/S008R03.npy"
SAMPLE_RATE = 190
sigs = ["Af7", "Af8", "T9", "T10"]

def main():
    plt.rcParams['figure.constrained_layout.use'] = True

    sig_list = [ALL_CHANNELS[i] for i in sigs]

    all_data = np.load(FILE)
    data = np.sum(all_data[:,[sig_list]], axis=-1).flatten()
    ica = FastICA(n_components=4)
    transformed_data = ica.fit_transform(all_data[:,sig_list])
    win_size = SAMPLE_RATE
    pe = ProcessEEG(win_size, SAMPLE_RATE)

    if not check_COLA(pe.window, win_size, win_size//2):
        print("Bad COLA")
        return
    fragments = data.shape[0]//pe.part_size-1

    iss, bs, ds = [], [], []

    _, infos, bands, data_rev = pe.process_all(all_data[:,sig_list[2]], True)
    iss.append(infos)
    bs.append(bands)
    ds.append(data_rev)
    for i in range(4):
        data = transformed_data[:, i]
        _, infos, bands, data_rev = pe.process_all(data, True)
        iss.append(infos)
        bs.append(bands)
        ds.append(data_rev)

    fig, ax = plt.subplots(4, 2, sharex=True)
    x = np.arange(data.shape[0]) / SAMPLE_RATE
    x_ids = (x >= 10) & (x <= 20)
    x2 = np.arange(fragments) * (win_size//2) / SAMPLE_RATE
    x_ids2 = (x2 >= 10) & (x2 <= 20)
    ax[3,0].set_xlabel("Raw signal")
    ax[3,1].set_xlabel("ICA")
    for i in range(4):
        ax[i,0].plot(x[x_ids], all_data[x_ids, sig_list[i]])
        ax[i,1].plot(x[x_ids], transformed_data[x_ids,i])
        # ax[i,1].plot(x[x_ids], ds[i+1][2, x_ids])
        # ax[i,2].plot(x2[x_ids2], bs[i+1][2, x_ids2] / np.sum(bs[i+1][:5, x_ids2], axis=0))
        # ax[i,2].set_ylim([0, 0.5])
        # ax[i,1].set_title("Alpha waves")
    # ax[4,0].plot(x[x_ids], all_data[x_ids,sig_list[3]])
    # ax[4,1].plot(x[x_ids], ds[0][2, x_ids])
    # ax[4,2].plot(x2[x_ids2], bs[0][2, x_ids2] / np.sum(bs[0][:5, x_ids2], axis=0))
    # ax[4,1].set_title("Alpha waves")
    print((np.abs(data-data_rev[-1]))[x_ids].sum() < 0.0001)

    x = np.arange(fragments) * (win_size//2) / SAMPLE_RATE
    x_ids = (x >= 10) & (x <= 20)
    # ax[1,1].plot(x[x_ids], bands[1, x_ids] / np.sum(bands[:5, x_ids], axis=0))
    # ax[1,1].set_ylim([0, 1])
    # ax[1,1].set_title("Teta")
    # ax[2,1].plot(x[x_ids], bands[2, x_ids] / np.sum(bands[:5, x_ids], axis=0))
    # ax[2,1].set_ylim([0, 1])
    # ax[2,1].set_title("Alpha")
    # ax[3,1].plot(x[x_ids], bands[3, x_ids] / np.sum(bands[:5, x_ids], axis=0))
    # ax[3,1].set_ylim([0, 1])
    # ax[3,1].set_title("Beta")
    # ax[1,2].plot(x[x_ids], (bands[1, x_ids]+bands[2, x_ids]) / np.sum(bands[:5, x_ids], axis=0))
    # ax[1,2].set_ylim([0, 1])
    # ax[1,2].set_title("(Teta + Alpha)")
    # ax[2,2].plot(x[x_ids], (bands[1, x_ids]+bands[3, x_ids]) / np.sum(bands[:5, x_ids], axis=0))
    # ax[2,2].set_ylim([0, 1])
    # ax[2,2].set_title("(Teta + Beta)")
    # ax[3,2].plot(x[x_ids], (bands[2, x_ids]+bands[3, x_ids]) / np.sum(bands[:5, x_ids], axis=0))
    # ax[3,2].set_ylim([0, 1])
    # ax[3,2].set_title("(Alpha + Beta)")
    fig.savefig("Plot.png")
    plt.show()

def main_bands_demo():
    plt.rcParams['figure.constrained_layout.use'] = True

    sig_list = [ALL_CHANNELS[i] for i in sigs]

    all_data = np.load(FILE)
    data = np.sum(all_data[:,[sig_list]], axis=-1).flatten()
    win_size = SAMPLE_RATE
    pe = ProcessEEG(win_size, SAMPLE_RATE)

    if not check_COLA(pe.window, win_size, win_size//2):
        print("Bad COLA")
        return
    fragments = data.shape[0]//pe.part_size-1

    iss, bs, ds = [], [], []

    for i in range(4):
        data = all_data[:, sig_list[i]]
        _, infos, bands, data_rev = pe.process_all(data, True)
        iss.append(infos)
        bs.append(bands)
        ds.append(data_rev)

    fig, ax = plt.subplots(4, 5, sharex=True)
    x = np.arange(data.shape[0]) / SAMPLE_RATE
    x_ids = (x >= 10) & (x <= 20)
    x2 = np.arange(fragments) * (win_size//2) / SAMPLE_RATE
    x_ids2 = (x2 >= 10) & (x2 <= 20)
    ax[3,0].set_xlabel("Raw signal")
    ax[3,1].set_xlabel("ICA")
    for i in range(4):
        ax[i,0].plot(x[x_ids], all_data[x_ids, sig_list[i]])
        for j in range(4):
            ax[i,j+1].plot(x[x_ids], data_rev[x_ids, i])
        # ax[i,1].plot(x[x_ids], ds[i+1][2, x_ids])
        # ax[i,2].plot(x2[x_ids2], bs[i+1][2, x_ids2] / np.sum(bs[i+1][:5, x_ids2], axis=0))
        # ax[i,2].set_ylim([0, 0.5])
        # ax[i,1].set_title("Alpha waves")
    # ax[4,0].plot(x[x_ids], all_data[x_ids,sig_list[3]])
    # ax[4,1].plot(x[x_ids], ds[0][2, x_ids])
    # ax[4,2].plot(x2[x_ids2], bs[0][2, x_ids2] / np.sum(bs[0][:5, x_ids2], axis=0))
    # ax[4,1].set_title("Alpha waves")
    print((np.abs(data-data_rev[-1]))[x_ids].sum() < 0.0001)

    x = np.arange(fragments) * (win_size//2) / SAMPLE_RATE
    x_ids = (x >= 10) & (x <= 20)
    # ax[1,1].plot(x[x_ids], bands[1, x_ids] / np.sum(bands[:5, x_ids], axis=0))
    # ax[1,1].set_ylim([0, 1])
    # ax[1,1].set_title("Teta")
    # ax[2,1].plot(x[x_ids], bands[2, x_ids] / np.sum(bands[:5, x_ids], axis=0))
    # ax[2,1].set_ylim([0, 1])
    # ax[2,1].set_title("Alpha")
    # ax[3,1].plot(x[x_ids], bands[3, x_ids] / np.sum(bands[:5, x_ids], axis=0))
    # ax[3,1].set_ylim([0, 1])
    # ax[3,1].set_title("Beta")
    # ax[1,2].plot(x[x_ids], (bands[1, x_ids]+bands[2, x_ids]) / np.sum(bands[:5, x_ids], axis=0))
    # ax[1,2].set_ylim([0, 1])
    # ax[1,2].set_title("(Teta + Alpha)")
    # ax[2,2].plot(x[x_ids], (bands[1, x_ids]+bands[3, x_ids]) / np.sum(bands[:5, x_ids], axis=0))
    # ax[2,2].set_ylim([0, 1])
    # ax[2,2].set_title("(Teta + Beta)")
    # ax[3,2].plot(x[x_ids], (bands[2, x_ids]+bands[3, x_ids]) / np.sum(bands[:5, x_ids], axis=0))
    # ax[3,2].set_ylim([0, 1])
    # ax[3,2].set_title("(Alpha + Beta)")
    fig.savefig("Plot.png")
    plt.show()

def main_ica():
    plt.rcParams['figure.constrained_layout.use'] = True

    sigs = ["Af7", "Af8", "T9", "T10"]
    sig_list = [ALL_CHANNELS[i] for i in sigs]

    all_data = np.load(FILE)
    data = np.sum(all_data[:,[sig_list]], axis=-1).flatten()
    ica = FastICA(n_components=4)
    transformed_data = ica.fit_transform(all_data[:,sig_list])
    win_size = SAMPLE_RATE
    pe = ProcessEEG(win_size, SAMPLE_RATE)

    fig, ax = plt.subplots(4, 2, sharex=True)
    x = np.arange(data.shape[0]) / SAMPLE_RATE
    x_ids = (x >= 10) & (x <= 20)
    ax[3,0].set_xlabel("Raw signal")
    ax[3,1].set_xlabel("ICA")
    for i in range(4):
        ax[i,0].plot(x[x_ids], all_data[x_ids, sig_list[i]])
        ax[i,1].plot(x[x_ids], transformed_data[x_ids,i])
    fig.savefig("ICA_demo.png")
    plt.show()

def main3():
    plt.rcParams['figure.constrained_layout.use'] = True

    sigs = ["Af7", "Af8", "Tp7", "Tp8"]
    sig_list = [ALL_CHANNELS[i] for i in sigs]

    all_data = np.load(FILE)
    data = np.sum(all_data[:,[sig_list]], axis=-1).flatten()
    win_size = SAMPLE_RATE
    pe = ProcessEEG(win_size, SAMPLE_RATE)

    if not check_COLA(pe.window, win_size, win_size//2):
        print("Bad COLA")
        return
    fragments = data.shape[0]//pe.part_size-1

    _, infos, bands, data_rev = pe.process_all(data, True)

    fig, ax = plt.subplots(4, 3, sharex=True)
    x = np.arange(data.shape[0]) / SAMPLE_RATE
    x_ids = (x >= 10) & (x <= 20)
    ax[0,0].plot(x[x_ids], data[x_ids])
    ax[0,0].set_title("Raw wave signal")
    ax[1,0].plot(x[x_ids], data_rev[1, x_ids])
    ax[1,0].set_title("Teta waves")
    ax[2,0].plot(x[x_ids], data_rev[2, x_ids])
    ax[2,0].set_title("Alpha waves")
    ax[3,0].plot(x[x_ids], data_rev[3, x_ids])
    ax[3,0].set_title("Beta waves")
    print((np.abs(data-data_rev[-1]))[x_ids].sum() < 0.0001)

    x = np.arange(fragments) * (win_size//2) / SAMPLE_RATE
    x_ids = (x >= 10) & (x <= 20)
    ax[1,1].plot(x[x_ids], bands[1, x_ids] / np.sum(bands[:5, x_ids], axis=0))
    ax[1,1].set_ylim([0, 1])
    ax[1,1].set_title("Teta")
    ax[2,1].plot(x[x_ids], bands[2, x_ids] / np.sum(bands[:5, x_ids], axis=0))
    ax[2,1].set_ylim([0, 1])
    ax[2,1].set_title("Alpha")
    ax[3,1].plot(x[x_ids], bands[3, x_ids] / np.sum(bands[:5, x_ids], axis=0))
    ax[3,1].set_ylim([0, 1])
    ax[3,1].set_title("Beta")
    ax[1,2].plot(x[x_ids], (bands[1, x_ids]+bands[2, x_ids]) / np.sum(bands[:5, x_ids], axis=0))
    ax[1,2].set_ylim([0, 1])
    ax[1,2].set_title("(Teta + Alpha)")
    ax[2,2].plot(x[x_ids], (bands[1, x_ids]+bands[3, x_ids]) / np.sum(bands[:5, x_ids], axis=0))
    ax[2,2].set_ylim([0, 1])
    ax[2,2].set_title("(Teta + Beta)")
    ax[3,2].plot(x[x_ids], (bands[2, x_ids]+bands[3, x_ids]) / np.sum(bands[:5, x_ids], axis=0))
    ax[3,2].set_ylim([0, 1])
    ax[3,2].set_title("(Alpha + Beta)")
    fig.savefig("Plot.png")
    plt.show()

def main2():
    plt.rcParams['figure.constrained_layout.use'] = True

    sigs = ["Af7", "Af8", "Tp7", "Tp8"]
    sig_list = [ALL_CHANNELS[i] for i in sigs]
    all_data = [np.zeros((6,1)) for _ in sigs]

    win_size = SAMPLE_RATE
    pe = ProcessEEG(win_size, SAMPLE_RATE)

    for i in range(1, 110):
        for j in range(2, 3):
            for kk, k in enumerate(sig_list):
                data = np.load(f"data/S{i:03d}R{j:02d}.npy")[:,k]
                _, infos, bands, data_rev = pe.process_all(data, False)
                # mean = np.mean(bands, axis=1).reshape(5,1)
                bands /= np.sum(bands[:5], axis=0)
                all_data[kk] = np.concatenate((all_data[kk], bands), axis=1)

    fig, ax = plt.subplots(2, 2, sharey=True)
    # all_data = 20*np.log10(all_data+1)
    for i in range(2):
        for j in range(2):
            kk = i*2+j
            ax[i,j].violinplot(all_data[kk][:5,:].transpose())
            ax[i,j].set_xticks(np.arange(1,6), labels=["Delta", "Theta", "Alpha", "Beta", "Gamma"])
            ax[i,j].set_title(sigs[kk])
            ax[i,j].set_ylabel("Volume, dB")
    fig.savefig("Powers2.png")
    plt.show()

if __name__ == "__main__":
    main_ica()
