import matplotlib.pyplot as plt
import numpy as np
import json
import math
from scipy import signal
from numpy import linalg as LA

from common.params import ALL_CHANNELS, DEMO_CHANNELS, DEMO_S, SAMPLE_RATE, RQA_PARAMS, EPS
from common.rqa_analyse import calc_rp, calc_rqa
from common import filter_noise_min
from common.process_eeg import bandpass_freq_smooth

def main():
    if input("Are you sure? (y/n)") != "y":
        return
    plt.rcParams['figure.constrained_layout.use'] = True
    plt.close('all')
    window = int(SAMPLE_RATE * 10)
    channels = [ALL_CHANNELS[i] for i in DEMO_CHANNELS]
    s = 8
    r = 1
    e = 2
    for r in [1, 2]:
        file = f"data/S{s:03d}R{r:02d}.npy"
        data = np.load(file)
        f, t, Zxx = signal.stft(data[:,channels[e]], SAMPLE_RATE, nperseg=window, noverlap=window-1)
        noise_free = filter_noise_min(f)
        alpha = bandpass_freq_smooth(f, 7, 14, 2)
        Zxx = np.apply_along_axis(lambda x: x * noise_free, 0, Zxx)
        Zxx_alpha = np.apply_along_axis(lambda x: x * alpha, 0, Zxx)
        _, out = signal.istft(Zxx, SAMPLE_RATE, nperseg=window, noverlap=window-1)
        # plt.plot(np.abs(Zxx).sum(axis=0))
        # plt.plot((np.abs(Zxx_alpha).sum(axis=0)/alpha.sum())/np.abs(Zxx).mean(axis=0))
        div = np.abs(Zxx).sum(axis=0)
        div[div == 0] = 1
        alpha_val = (np.abs(Zxx_alpha).sum(axis=0)) / div * 100
        plt.plot(alpha_val, label=f"{r} a {alpha_val.mean():.2f}")
        # plt.plot(f, np.abs(Zxx).mean(axis=1))
        # plt.plot(np.abs(Zxx).mean(axis=0), label=f"{r}")
    plt.legend()
    print(np.max(out)-np.min(out), np.abs(Zxx).sum(axis=0).max())
    plt.plot()

if __name__ == "__main__":
    main()