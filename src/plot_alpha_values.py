#!/usr/bin/env python3
from scipy.signal import check_COLA
import numpy as np
import matplotlib.pyplot as plt

from common.params import ALL_CHANNELS, DEMO_CHANNELS, DEMO_S, SAMPLE_RATE
from common.process_eeg import ProcessEEG

FILES = [f"data/{DEMO_S}R02.npy", f"data/{DEMO_S}R03.npy"]

def main():
    plt.rcParams['figure.constrained_layout.use'] = True

    sig_list = [ALL_CHANNELS[i] for i in DEMO_CHANNELS]

    all_data = [np.load(i) for i in FILES]
    data = [i[:, sig_list] for i in all_data]
    win_size = SAMPLE_RATE
    pe = ProcessEEG(win_size, SAMPLE_RATE)

    if not check_COLA(pe.window, win_size, win_size//2):
        print("Bad COLA")
        return

    iss, bs, ds = [], [], []

    for j in range(2):
        for i in range(4):
            _, infos, bands, data_rev = pe.process_all(data[j][:,i], True)
            iss.append(infos)
            bs.append(bands)
            ds.append(data_rev)

    fig, ax = plt.subplots(4, 2, sharex=True)

    x_filt = lambda x: (x >= 10) & (x <= 20)
    x = [np.arange(i.shape[0]) / SAMPLE_RATE for i in data]
    x_ids = [x_filt(i) for i in x]
    x_win_scale = pe.part_size / SAMPLE_RATE
    xb = [np.arange(i.shape[1]) * x_win_scale for i in bs[::4]]
    xb_ids = [x_filt(i) for i in xb]

    states = ["Closed eyes", "Opened eyes"]

    ax[3,0].set_xlabel("Raw signal, s")
    ax[3,1].set_xlabel("Alpha power score, s")
    for i in range(4):
        ax[i,0].set_ylabel(f"{DEMO_CHANNELS[i]}, uV")
        for j in range(2):
            ax[i,0].plot(x[j][x_ids[j]], data[j][x_ids[j], i], alpha=0.9, label=states[j])
            ax[i,0].legend()
            b = bs[j*4+i][:5, xb_ids[j]]
            ax[i,1].plot(xb[j][xb_ids[j]], b[2] / np.sum(b, axis=0), label=states[j])
            ax[i,1].legend()
            ax[i,1].set_ylim([0,1])
            # ax[4,i].set_xlabel(DEMO_CHANNELS[i])

    fig.set_size_inches(30//2,18//2)
    # fig.savefig("figs/Alpha_power.png")
    plt.show()

if __name__ == "__main__":
    main()
