#!/usr/bin/env python3
from scipy.signal import check_COLA
import numpy as np
import matplotlib.pyplot as plt

from common.params import ALL_CHANNELS, DEMO_CHANNELS, DEMO_S, SAMPLE_RATE
from common.process_eeg import ProcessEEG

FILE = f"data/{DEMO_S}R03.npy"

def main():
    plt.rcParams['figure.constrained_layout.use'] = True

    sig_list = [ALL_CHANNELS[i] for i in DEMO_CHANNELS]

    all_data = np.load(FILE)
    data = all_data[:, sig_list]
    win_size = SAMPLE_RATE
    pe = ProcessEEG(win_size, SAMPLE_RATE)

    if not check_COLA(pe.window, win_size, win_size//2):
        print("Bad COLA")
        return
    fragments = data.shape[0]//pe.part_size-1

    iss, bs, ds = [], [], []

    for i in range(4):
        _, infos, bands, data_rev = pe.process_all(data[:,i], True)
        iss.append(infos)
        bs.append(bands)
        ds.append(data_rev)

    fig, ax = plt.subplots(5, 4, sharex=True)
    x = np.arange(data.shape[0]) / SAMPLE_RATE
    x_ids = (x >= 10) & (x <= 12)

    ax[0,0].set_ylabel("Raw signal")
    ax[1,0].set_ylabel("Delta waves")
    ax[2,0].set_ylabel("Theta waves")
    ax[3,0].set_ylabel("Alpha waves")
    ax[4,0].set_ylabel("Beta waves")

    for i in range(4):
        ax[0,i].plot(x[x_ids], data[x_ids, i])
        for j in range(4):
            ax[j+1,i].plot(x[x_ids], ds[i][j,x_ids])
        ax[4,i].set_xlabel(DEMO_CHANNELS[i])

    fig.set_size_inches(30//2,18//2)
    # fig.savefig("figs/Band_waves.png")
    plt.show()

if __name__ == "__main__":
    main()
