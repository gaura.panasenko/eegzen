#!/usr/bin/env python3
import numpy as np
import matplotlib.pyplot as plt
from scipy.signal import welch

from common.params import EPS, ALL_CHANNELS, DEMO_CHANNELS, DEMO_S, SAMPLE_RATE

DEMO_S = "S043"
FILE = f"data/{DEMO_S}R01.npy"

def main():
    plt.rcParams['figure.constrained_layout.use'] = True

    sig_list = [ALL_CHANNELS[i] for i in DEMO_CHANNELS]

    all_data = np.load(FILE)
    data = all_data[:, sig_list]

    fig, ax = plt.subplots(4, 2)
    x = np.arange(data.shape[0]) / SAMPLE_RATE
    x_ids = (x >= 10) & (x <= 20)
    ax[3,0].set_xlabel("Raw signal, sec")
    ax[3,1].set_xlabel("Frequency, Hz")
    for i in range(4):
        ax[i,0].set_ylabel(f"{DEMO_CHANNELS[i]}, uV")
        dt = data[x_ids, i]
        ax[i,0].plot(x[x_ids], dt)
        ax[i,1].set_ylabel(f"{DEMO_CHANNELS[i]}, uV^2/Hz")
        f, psd = welch(dt, SAMPLE_RATE, nperseg=SAMPLE_RATE)
        ax[i,1].semilogy(f, psd)
        ax[i,1].fill_between(f[(f >= 0.5) & (f <= 4)], psd[(f >= 0.5) & (f <= 4)], label="Delta")
        ax[i,1].fill_between(f[(f >= 4) & (f <= 8)], psd[(f >= 4) & (f <= 8)], label="Theta")
        ax[i,1].fill_between(f[(f >= 7.5) & (f <= 13)], psd[(f >= 7.5) & (f <= 13)], label="Alpha")
        ax[i,1].fill_between(f[(f >= 13) & (f <= 30)], psd[(f >= 13) & (f <= 30)], label="Beta")
        ax[i,1].fill_between(f[(f >= 30)], psd[(f >= 30)], label="Gamma")
        ax[i,1].legend()
    fig.set_size_inches(30//2,18//2)
    fig.savefig("figs/Welch_demo_R01.png")
    plt.show()

if __name__ == "__main__":
    main()
