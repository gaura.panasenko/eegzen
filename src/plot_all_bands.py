#!/usr/bin/env python3
import numpy as np
import matplotlib.pyplot as plt

from common.params import ALL_CHANNELS, DEMO_CHANNELS2, SAMPLE_RATE
from common.process_eeg import ProcessEEG


def main():
    plt.rcParams['figure.constrained_layout.use'] = True

    sig_list = [ALL_CHANNELS[i] for i in DEMO_CHANNELS2]

    pe = ProcessEEG(SAMPLE_RATE, SAMPLE_RATE)

    alphas1 = [[] for i in range(4)]
    alphas2 = [[] for i in range(4)]
    alphas3 = [[] for i in range(4)]
    for i in range(1, 110):
        for j in range(1, 5):
            file = f"data/S{i:03d}R{j:02d}.npy"
            all_data = np.load(file)
            data = all_data[:, sig_list]
            t, bands, infos = pe.process_full(data)
            alphas1[j-1].append((infos[0]))
            alphas2[j-1].append((infos[1]))
            alphas3[j-1].append((infos[2]))

    for i in range(4):
        alphas1[i] = np.concatenate(alphas1[i]).flatten()
        alphas2[i] = np.concatenate(alphas2[i]).flatten()
        alphas3[i] = np.concatenate(alphas3[i]).flatten()

    fig, ax = plt.subplots(3, 1)

    print("Calculated")

    bs = []
    bs2 = []
    for j in range(1, 5):
        file = f"data/S028R{j:02d}.npy"
        all_data = np.load(file)
        data = all_data[:, sig_list]
        t, bands, infos = pe.process_full(data)
        bs.append((j, t, infos[0]))
        bs2.append((j, t, infos[1]))
    # alphas_relax.append((infos[0]))

    ax[0].violinplot(alphas1)
    ax[1].violinplot(alphas2)
    ax[2].violinplot(alphas3)
    ax[0].set_ylim([-0.1,1.1])
    ax[1].set_ylim([-0.1,1.1])
    ax[2].set_ylim([-0.1,1.1])
    # ax[1].set_ylim([-0.1,1.1])
    # for i, t, rel in bs:
    #     ax[2].plot(t, rel, label=i)
    # ax[2].set_xlim([10,50])
    # ax[2].legend()
    # for i, t, rel in bs2:
    #     ax[3].plot(t, rel, label=i)
    # ax[3].set_xlim([10,50])
    # ax[3].legend()
    fig.set_size_inches(30//2,18//2)
    # fig.savefig("figs/ICA_demo.png")
    plt.show()

if __name__ == "__main__":
    main()

