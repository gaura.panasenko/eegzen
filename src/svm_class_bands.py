import numpy as np
from sklearn.preprocessing import StandardScaler, label_binarize
from sklearn.svm import SVC

from common.params import ALL_CHANNELS, DEMO_CHANNELS, DEMO_S, SAMPLE_RATE, RQA_PARAMS

def main():
    data = np.load(f"bands.npy")


    Ss = 109 + 1

    inv_channel = {v: k for k, v in ALL_CHANNELS.items()}
    for channel in DEMO_CHANNELS:
        kk = ALL_CHANNELS[channel]
        out_data = np.zeros((Ss * 25 * 2, 10))
        labels = np.zeros((Ss * 25 * 2,))
        labels[Ss * 25:] = 1
        test_data = out_data.copy()
        for s in range(1, Ss):
            for r in (1, 2):
                kkk = (r-1) * Ss * 25 + (s-1) * 25
                dt = data[s-1, r-1, kk, :, :]
                out_data[kkk:kkk+25, :] = dt[:25,:]
                test_data[kkk:kkk+25, :] = dt[25:,:]
        
        scaler = StandardScaler()
        scaler.fit_transform(out_data)
        scaled = scaler.transform(test_data)
        test_scaled = scaler.transform(out_data)
        clf = SVC(kernel="rbf", C=100, gamma=1)
        clf.fit(scaled, labels)
        print(f"{channel:5s}|{clf.score(test_scaled, labels) * 100:10.2f}")


if __name__ == "__main__":
    main()