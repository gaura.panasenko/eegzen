import matplotlib.pyplot as plt
import numpy as np
import json
import math

from common.params import ALL_CHANNELS

descrs = {
    'tau': "Time delay (tau)", 
    'm': "Embedded dimension (m)", 
    'eps': "Epsilon (eps)", 
    'L_min': "Minimum diagonal line length (L_min)", 
    'V_min': "Minimum vertical line length (V_min)", 
    'W_min': "Minimum white vertical line length (W_min)", 
    'RR': "Recurrence rate (RR)", 
    'DET': "Determinism (DET)", 
    'L': "Average diagonal line length (L)", 
    'L_max': "Longest diagonal line length (L_max)", 
    'DIV': "Divergence (DIV)", 
    'L_entr': "Entropy diagonal lines (L_entr)", 
    'LAM': "Laminarity (LAM)", 
    'TT': "Trapping time (TT)", 
    'V_max': "Longest vertical line length (V_max)", 
    'V_entr': "Entropy vertical lines (V_entr)", 
    'W': "Average white vertical line length (W)", 
    'W_max': "Longest white vertical line length (W_max)", 
    'W_div': "Longest white vertical line length divergence (W_div)", 
    'W_entr': "Entropy white vertical lines (W_entr)", 
    'DET/RR': "Ratio determinism / recurrence rate (DET/RR)", 
    'LAM/DET': "Ratio laminarity / determinism (LAM/DET)"
}

def main():
    with open("rqa.json", "r") as f:
        d = json.load(f)

    lbls = list(next(iter(d.values())).keys())
    lbls_size = len(lbls)
    # data = np.zeros((lbls_size*3,110,3), dtype=np.float64)

    # for l in range(lbls_size):
    #     for i in range(1, 110):
    #         i_name = f"S{i:03d}"
    #         for j in range(1, 4):
    #             j_name = f"R{j:02d}"
    #             name = f"{i_name}{j_name}"
    #             for k in range(3):
    #                 signame = ALL_CHANNELS[k]
    #                 key = f"{name}_{signame}"
    #                 if key in d:
    #                     val = d[key][lbls[l]]
    #                     data[l*k,i,(j-1)] = val

    plt.close('all')

    for l in range(lbls_size):
        t = f"{lbls[l]}"
        fig, ax = plt.subplots(4, 1, num=t, layout="constrained")
        fig.suptitle(descrs[t])
        for kk, signame in enumerate(["Fp1", "Fp2", "O1", "O2"]):
            k = ALL_CHANNELS[signame]
            out_range = np.arange(1, 5)
            out_data = [[] for i in out_range]
            for i in range(1, 110):
                i_name = f"S{i:03d}"
                for j in range(1, 5):
                    # for xx in range(0, 3):
                        j_name = f"R{j:02d}"
                        name = f"{i_name}{j_name}"
                        key = f"{name}_{signame}"
                        if key in d:
                            val = d[key][lbls[l]]
                            if not(math.isnan(val) or math.isinf(val) or math.isclose(val, 0)):
                                out_data[(j-1)].append(val)
            #ax[k].violinplot(data[l*k])
            if not out_data:
                continue
            for j in out_range:
                if len(out_data[j-1]) == 0:
                    out_data[j-1].append(0)
            ax[kk].violinplot(out_data)
            fig_labels = [f"State R{j:02d}\n{len(out_data[j-1])} people" for j in out_range]
            ax[kk].set_xticks(out_range, labels=fig_labels)
            ax[kk].set_title(f"{signame}")
            #ax[k].set_xlabel([f"R{j:02d}" for i in range(1,4)])
        fig.savefig(f"figs/{t.replace('/', '_')}.png")
    plt.show()

if __name__ == "__main__":
    main()