import matplotlib.pyplot as plt
import numpy as np
import json
import math
from scipy import signal
from numpy import linalg as LA

from common.params import ALL_CHANNELS, DEMO_CHANNELS, DEMO_S, SAMPLE_RATE, RQA_PARAMS, EPS
from common.rqa_analyse import calc_rp, calc_rqa
from teaspoon.parameter_selection.FNN_n import FNN_n
from teaspoon.parameter_selection.MI_delay import MI_for_delay

from common import filter_noise_min

def norm(x):
    return np.sqrt(np.mean(x**2)) * 2

def main():
    plt.rcParams['figure.constrained_layout.use'] = True
    plt.rcParams['text.usetex'] = True
    plt.close('all')
    window = int(SAMPLE_RATE * 5) * 2
    w = f"{window / SAMPLE_RATE:0.2f}"
    channel = ALL_CHANNELS[DEMO_CHANNELS[2]]
    dt = []

    s = "S001"

    j_names = ["Opened eyes", "Closed eyes"]
    
    for j in range(1, 3):
        file = f"data/{s}R{j:02d}.npy"
        data = np.load(file)[:, channel]
        f, t, Zxx = signal.stft(data, SAMPLE_RATE, nperseg=window)
        noise_free = filter_noise_min(f)
        Zxx = np.apply_along_axis(lambda x: x * noise_free, 0, Zxx)
        _, out = signal.istft(Zxx, SAMPLE_RATE, nperseg=window)
        dt.append(out)

    # dt_norm = norm(np.concatenate(dt))
    # print(dt_norm)

    # for j in dt:
    #     j /= 200
    # data[:, channel] /= LA.norm(data[:, channel])
    # data[:, channel] = signal.detrend(data[:, channel])

    Ls = [[] for _ in dt]

    for j, jd in enumerate(dt):
        # fig, ax = plt.subplots(2, 3, sharex=True, sharey=True)
        wins = (jd.shape[0] - window) // (window // 2)

        for i in range(wins):
            start = i * (window // 2)
            x = jd[start:start + window]
            xx, yy = np.meshgrid(x, x)
            eps = float(np.abs(xx - yy).mean())
            # x /= norm(x)
            tau = MI_for_delay(x)
            m = FNN_n(x, tau)[1]
            rqa = calc_rqa(x, 4, 3, eps)
            L = rqa['L']
            Ls[j].append(L)
            if i // 2 < 6 and i % 2 == 0:
                # ax_i = ax[i//2//3,i//2%3]
                print(x.shape)
                plt.imshow(calc_rp(x, 4, 3, 0, 1, window, eps, None), cmap="Grays")
                break
                # ax_i.set_title(f"$\\epsilon={eps:0.2f}, m={m}, \\frac{{DET}}{{RR}}={L:0.2f}$")
        # fig.suptitle(f"{s} - $w$={w} - {j_names[j]}")
        # fig.set_size_inches(30//2,18//2)
        # fig.savefig(f"figs/{s}R{j+1:02d}_w{w}_CRP.png")

    
    fig, ax = plt.subplots(1, 1, sharex=True, sharey=True)
    fig.suptitle(f"{s} - $w$={w} - {RQA_PARAMS['DET/RR']}")
    for i in range(len(dt)):
        Lx = np.arange(len(Ls[i])) * window / SAMPLE_RATE
        ax.plot(Lx, Ls[i], alpha=0.7, label=f"{j_names[i]}, $DET/RR_{{mean}}$={np.mean(Ls[i]):0.2f}")
    ax.set_xlabel("Time, s")
    ax.legend()
    # fig.set_size_inches(30//2,18//2)
    # fig.savefig(f"figs/{s}_w{w}_L.png")

    # fig, ax = plt.subplots(1, 1, sharex=True, sharey=True)
    # for i in dt:
    #     ax.plot(i, alpha=0.7)
    # fig.set_size_inches(30//2,18//2)
    # fig.savefig(f"figs/{s}_sigs.png")

    plt.show()


if __name__ == "__main__":
    main()
