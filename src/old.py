
def gen_file_name(i, j):
    return f"S{i:03d}R{j:02d}"

def get_all_w(d, j):
    for i in range(1, 110):
        key = f"{gen_file_name(i, j)}_Fp2"
        yield d[key]["W"]

    # k = 2 #Fp2
    # my_people = [0, 50, 96, 80, 99]
    # min_i = 50
    # max_i = 32
    # avg_i = 29

    # for j in range(1, 5):
        # min_j = 0
        # max_j = 0
        # vals = list(get_all_w(d, j))
        # max_i = np.argmax(vals)
        # min_i = np.argmin(vals)
        # avg = np.mean(vals)
        # avg_i = np.argmin(np.abs(vals - avg))
        # print(min_i, max_i, avg, avg_i)
        # min_name = gen_file_name(min_i, j)
        # max_name = gen_file_name(max_i, j)
        # avg_name = gen_file_name(avg_i, j)
        # x = np.load(f"data/{min_name}.npy")[:,CHANNEL]
        # x /= np.abs(x).max()
        # print(x.shape)
        # start, step, end = 0, 1, 9000
        # calc_rp(x, 1, 4, eps=EPS, start=start, step=step, end=end, plotting=True, title=f"{min_name}")
        # plt.show()
        # x = np.load(f"data/{max_name}.npy")[:,CHANNEL]
        # x /= np.abs(x).max()
        # calc_rp(x, 1, 4, eps=EPS, start=start, step=step, end=end, plotting=True, title=f"{max_name}")
        # plt.show()
        # x = np.load(f"data/{avg_name}.npy")[:,CHANNEL]
        # x /= np.abs(x).max()
        # calc_rp(x, 1, 4, eps=EPS, start=start, step=step, end=end, plotting=True, title=f"{avg_name}")
        # plt.show()