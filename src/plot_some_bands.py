#!/usr/bin/env python3
import numpy as np
import matplotlib.pyplot as plt

from common.params import ALL_CHANNELS, DEMO_CHANNELS2, SAMPLE_RATE
from common.process_eeg import ProcessEEG


def main():
    plt.rcParams['figure.constrained_layout.use'] = True

    pe = ProcessEEG(SAMPLE_RATE, SAMPLE_RATE)

    fig, ax = plt.subplots(3, 1)

    file = f"data/Yan/1693053117.5295823_eeg.npy"
    all_data = np.load(file)
    data = all_data[[1,2,3,4], :].transpose()
    print(data.shape)
    t, bands, infos = pe.process_full(data)

    ax[0].plot(t, infos[0])
    ax[1].plot(t, infos[1])
    ax[2].plot(t, infos[2])
    ax[0].set_ylim([-0.05, 1.05])
    ax[1].set_ylim([-0.05, 1.05])
    ax[2].set_ylim([-0.05, 1.05])
    fig.set_size_inches(30//2,18//2)
    # fig.savefig("figs/ICA_demo.png")
    plt.show()

if __name__ == "__main__":
    main()

