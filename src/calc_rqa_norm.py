#!/usr/bin/env python3
from curses import window
import os
import matplotlib.pyplot as plt
import numpy as np
from numpy.fft import rfft, irfft, rfftfreq
import json
import math

from scipy import signal

from common.rqa_analyse import calc_rqa_tuple, calc_rqa_windowed
from common.params import EPS, ALL_CHANNELS, DEMO_CHANNELS, SAMPLE_RATE, RQA_ORDER
from common import filter_noise_min

# ["Af7", "Af8", "Tp7", "Tp8", "O1", "O2", "Fp1", "Fp2"]

nseg = int(SAMPLE_RATE * 5) * 2
f = rfftfreq(nseg, 1 / SAMPLE_RATE)
noise_free = filter_noise_min(f)
tau, m = 4, 3

def norm(x):
    return np.sqrt(np.mean(x**2)) * 2

def calc(name, filename):
    x = np.load(filename)
    nsegs = (x.shape[0] - nseg) // (nseg // 2)
    out = np.zeros((len(DEMO_CHANNELS), nsegs, len(RQA_ORDER)))
    out[:,:,:4] = tau, m, 0, nseg
    out_file = f"rqa/S_norm/{name}"
    if os.path.exists(f"{out_file}.npy"):
        return

    for c, channel in enumerate(DEMO_CHANNELS):
        e = ALL_CHANNELS[channel]
        x_e = x[:, e]
        x_mean = x_i = x_e.mean()
        x_std = np.std(x_e)
        x_e = (x_e - x_mean) / x_std
        for i in range(nsegs):
            x_i = x_e[i*nseg//2:i*nseg//2+nseg]
            x_i = irfft(rfft(x_i) * noise_free)
            # xx, yy = np.meshgrid(x_i, x_i)
            # eps = np.abs(xx - yy).mean()
            eps = 0.08
            out[c,i,2] = eps
            out[c,i,4:] = calc_rqa_tuple(x_i, tau, m, eps)
    np.save(out_file, out)

def main():
    if input("Are you sure? (y/n)") != "y":
        return
    for i in range(1, 110):
        i_name = f"S{i:03d}"
        # for j in range(3, 5):
        #     j_name = f"R{j:02d}"
        #     for k in range(3):
        #         name = f"{i_name}{j_name}T{k}"
        #         print(f"Processing {name}...")
        #         filename = f"data/T/{name}.npy"
        #         calc(name, filename, data)
        for j in range(1, 3):
            j_name = f"R{j:02d}"
            name = f"{i_name}{j_name}"
            print(f"Processing {name}...")
            filename = f"data/{name}.npy"
            calc(name, filename)

if __name__ == "__main__":
    main()