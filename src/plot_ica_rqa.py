from tarfile import data_filter
import matplotlib.pyplot as plt
import numpy as np
import json
import math
from scipy import signal
from numpy import linalg as LA

from common.params import ALL_CHANNELS, DEMO_CHANNELS, DEMO_S, SAMPLE_RATE, RQA_PARAMS, EPS
from common.rqa_analyse import calc_rp, calc_rqa
from teaspoon.parameter_selection.FNN_n import FNN_n
from teaspoon.parameter_selection.MI_delay import MI_for_delay
from sklearn.decomposition import FastICA

from common.process_eeg import ProcessEEG
import warnings
warnings.simplefilter("ignore")


def norm(x):
    return np.sqrt(np.mean(x**2)) * 2

def main():
    plt.rcParams['figure.constrained_layout.use'] = True
    plt.rcParams['text.usetex'] = True
    plt.close('all')
    window = int(SAMPLE_RATE * 0.5)
    w = f"{window / SAMPLE_RATE:0.2f}"
    channels = [ALL_CHANNELS[i] for i in DEMO_CHANNELS]
    # dt = []

    j_names = ["Opened eyes", "Closed eyes"]

    pe = ProcessEEG(SAMPLE_RATE, SAMPLE_RATE)
    ica = FastICA(n_components=4, random_state=0)
    Ls = [[] for _ in range(1, 3)]

    a1 = 0
    a2 = 0
    
    for i in range(1, 110):
        s = f"S{i:03d}"
        file = f"data/{s}R01.npy"
        data = np.load(file)
        data_ch = data[:, channels]
        data_filt = np.zeros(data_ch.shape)
        for k in range(data_ch.shape[1]):
            data_filt[:, k] = pe.filter_noise(data_ch[:, k])[1][:data.shape[0]]
        ica.fit(data_filt)
        print()
        for j in range(1, 3):
            file = f"data/{s}R{j:02d}.npy"
            data = np.load(file)
            data_ch = data[:, channels]
            data_filt = np.zeros(data_ch.shape)
            for k in range(data_ch.shape[1]):
                data_filt[:, k] = pe.filter_noise(data_ch[:, k])[1][:data.shape[0]]
            # dt.append(data_ch)
            transformed_data = ica.fit_transform(data_ch)
            A_ = ica.mixing_
            W_ =  ica.components_
            # assert np.allclose(data_ch, np.dot(transformed_data, A_.T) + ica.mean_)
            # print()
            # print(W_)
            order = [np.argmax(np.abs(W_[row])) for row in range(4)]
            # print(order)
            # print(np.argsort(np.sum(W_**2,axis=0)))
            # print(np.argsort(np.sum(W_**2,axis=1)))
            order = np.argsort(np.sum(W_**2,axis=1))

            # fig, ax = plt.subplots(4, 2, sharex=True)
            L_ij = []
            for k in range(2,3):
                # dt = transformed_data[:,order[k]] / 4
                dt = data_ch[:1024,k]
                mm = np.mean([np.max(np.abs(dt[i*SAMPLE_RATE:(i+1)*SAMPLE_RATE])) for i in range(dt.shape[0]//SAMPLE_RATE-1)])
                dt /= mm
                L = calc_rqa(dt, 1, 4, 0.1)["L"]
                L_ij.append(L)
                # ax[k,0].plot(data_ch[:,k])
                # ax[k,1].plot(dt)
            L_ij = np.array(L_ij[:])
            L_ij = L_ij[L_ij < 5]
            Ls[j-1].append(L_ij)
            L_val = round(np.mean(L_ij), 2)
            print(L_val, L_ij)
            if j == 1 and L_val > 2.9:
                a1+=1
            if j == 2 and L_val <= 2.9:
                a2+=1

    # for i in Ls:
    #     print(i)
    print(a1,a2)

    plt.show()


if __name__ == "__main__":
    main()