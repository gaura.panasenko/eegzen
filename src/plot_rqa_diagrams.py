import matplotlib.pyplot as plt
import numpy as np
import json

from common.rqa_analyse import calc_rp
from common.params import EPS, ALL_CHANNELS, DEMO_CHANNELS, DEMO_S


def main():
    plt.rcParams['figure.constrained_layout.use'] = True
    plt.close('all')

    files = [f"{DEMO_S}R01", f"{DEMO_S}R02", f"T/{DEMO_S}R03T0", f"T/{DEMO_S}R03T1", f"T/{DEMO_S}R03T2"]

    fig, ax = plt.subplots(4, 5, sharex=True, sharey=True)

    for ki, i in enumerate(files):
        for kj, j in enumerate(DEMO_CHANNELS):
            if ki == 0:
                ax[kj,ki].set_ylabel(j)
            x = np.load(f"data/{i}.npy")[:700, ALL_CHANNELS[j]]
            x /= np.abs(x).max()
            calc_rp(x, 1, 4, start=0, eps=EPS, plotting=ax[kj,ki], title=f"{i}_{j}")
    fig.set_size_inches(30//2,18//2)
    fig.savefig("figs/RQA_diagrams.png")
    plt.show()


if __name__ == "__main__":
    main()