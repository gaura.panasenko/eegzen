#!/usr/bin/env python3
import numpy as np
import matplotlib.pyplot as plt
from scipy.signal import welch

from common.params import EPS, ALL_CHANNELS, DEMO_CHANNELS, DEMO_S, SAMPLE_RATE

def main():
    plt.rcParams['figure.constrained_layout.use'] = True
    plt.close('all')

    sig_list = [ALL_CHANNELS[i] for i in DEMO_CHANNELS]

    for s in range(1, 110):
        file = f"data/S{s:03d}R01.npy"
        all_data = np.load(file)
        data_opened = all_data[:, sig_list]

        file = f"data/S{s:03d}R02.npy"
        all_data = np.load(file)
        data_closed = all_data[:, sig_list]

        length = min(data_closed.shape[0], data_opened.shape[0])
        data_opened = data_opened[:length,:]
        data_closed = data_closed[:length,:]

        fig, ax = plt.subplots(2, 2, sharex=True, sharey=True)
        fig.suptitle(f"S{s:03d} - Welch diagram")
        x = np.arange(length) / SAMPLE_RATE
        x_ids = (x >= 10) & (x <= 20)
        ax[1,0].set_xlabel("Opened eyes, Hz")
        ax[1,1].set_xlabel("Closed eyes, Hz")
        for i in range(2):
            dt = data_opened[x_ids, i+2]
            ax[i,0].set_ylabel(f"{DEMO_CHANNELS[i+2]}, uV^2/Hz")
            f, psd = welch(dt, SAMPLE_RATE, nperseg=SAMPLE_RATE)
            psd[(f >= 59) & (f <= 61)] = 0
            ax[i,0].plot(f, psd)
            ax[i,0].fill_between(f[(f >= 8) & (f <= 21)], psd[(f >= 8) & (f <= 21)], label="Alpha")
            dt = data_closed[x_ids, i+2]
            ax[i,1].set_ylabel(f"{DEMO_CHANNELS[i+2]}, uV^2/Hz")
            f, psd = welch(dt, SAMPLE_RATE, nperseg=SAMPLE_RATE)
            psd[(f >= 59) & (f <= 61)] = 0
            ax[i,1].plot(f, psd)
            ax[i,1].fill_between(f[(f >= 8) & (f <= 21)], psd[(f >= 8) & (f <= 21)], label="Alpha")
        fig.set_size_inches(30//2,18//2)
        fig.savefig(f"figs/S{s:03d}_Welch.png")
        plt.close()
    # plt.show()

if __name__ == "__main__":
    main()
