import matplotlib.pyplot as plt
import numpy as np

from common.params import RQA_ORDER, DEMO_CHANNELS, FFT_AVGS

plot_filt = (2,) + tuple(range(7, 18)) + tuple(range(19, 23))

def main():
    if input("Are you sure? (y/n)") != "y":
        return
    plt.rcParams['figure.constrained_layout.use'] = True
    plt.rcParams['text.usetex'] = True
    plt.close('all')
    x = np.load(f"alphas.npy")
    for s in range(1, 110):
        fig, ax = plt.subplots(4, 1, sharex=True, label=f"S{s:03d}")
        fig.suptitle(f"S{s:03d}")
        for e in range(4):
            channel = DEMO_CHANNELS[e]
            t = np.arange(x.shape[3])
            x_i = x[s-1,0,e,:]
            y_i = x[s-1,1,e,:]
            kwargs = {}
            kwargs["label"] = f"Closed eyes $R_1$={x_i.mean():0.2f}"
            ax[e].plot(t, x_i, **kwargs)
            kwargs["label"] = f"Opened eyes $R_2$={y_i.mean():0.2f}"
            ax[e].plot(t, y_i, **kwargs)
            ax[e].legend()
            ax[e].set_ylabel(f"{channel}")
            # ax[e].set_ylim((0, 100))
            ax[e].axhline(y=FFT_AVGS[e], color='0.6', linestyle='--')
            ax[e].set_xlabel("Time, s")
        fig.set_size_inches(30//2, 18//2)
        fig.savefig(f"figs/FFT_Alpha_S{s:03d}.png")
        plt.close()
        # plt.show()

if __name__ == "__main__":
    main()
