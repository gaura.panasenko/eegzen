#!/usr/bin/env python3
import matplotlib.pyplot as plt
import numpy as np
import json
import math

from scipy import signal

from common.rqa_analyse import calc_rqa_windowed
from common.params import EPS, ALL_CHANNELS, DEMO_CHANNELS, SAMPLE_RATE
from common import filter_noise_min

# ["Af7", "Af8", "Tp7", "Tp8", "O1", "O2", "Fp1", "Fp2"]

def norm(x):
    return np.sqrt(np.mean(x**2)) * 2

def calc(name, filename, data):
    x = np.load(filename)
    for channel in DEMO_CHANNELS:
        k = ALL_CHANNELS[channel]
        key = f"{name}_{channel}"
        if key in data:
            continue
        tau, m = 4, 3
        window = int(SAMPLE_RATE * 5) * 2
        f, t, Zxx = signal.stft(x[:,k], SAMPLE_RATE, nperseg=window)
        noise_free = filter_noise_min(f)
        Zxx = np.apply_along_axis(lambda x: x * noise_free, 0, Zxx)
        _, out = signal.istft(Zxx, SAMPLE_RATE, nperseg=window)
        res = calc_rqa_windowed(out, tau, m, window)
        dt = {"tau": tau, "m": m}
        dt.update(res)
        data[key] = dt

def main():
    if input("Are you sure? (y/n)") != "y":
        return
    try:
        with open('rqa/rqa.json', 'r', encoding='utf-8') as f:
            data = json.load(f)
    except FileNotFoundError:
        data = {}
    try:
        for i in range(1, 110):
            i_name = f"S{i:03d}"
            # for j in range(3, 5):
            #     j_name = f"R{j:02d}"
            #     for k in range(3):
            #         name = f"{i_name}{j_name}T{k}"
            #         print(f"Processing {name}...")
            #         filename = f"data/T/{name}.npy"
            #         calc(name, filename, data)
            for j in range(1, 3):
                j_name = f"R{j:02d}"
                name = f"{i_name}{j_name}"
                print(f"Processing {name}...")
                filename = f"data/{name}.npy"
                calc(name, filename, data)
    finally:
        with open('rqa/rqa.json', 'w', encoding='utf-8') as f:
            json.dump(data, f, ensure_ascii=False, indent=4)

if __name__ == "__main__":
    main()
