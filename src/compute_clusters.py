import numpy as np
import matplotlib.pyplot as plt
from sklearn.cluster import KMeans

from common.params import ALL_CHANNELS, DEMO_CHANNELS, DEMO_S, SAMPLE_RATE, RQA_PARAMS

def main():
    plt.rcParams['figure.constrained_layout.use'] = True
    plt.close('all')
    Ss = 109 + 1

    data = np.zeros((Ss-1, 2, 4, 10, 24))

    out = {}

    for s in range(1, Ss):
        for r in (1, 2):
            data[s-1, r-1 , :, :, :23] = np.load(f"rqa/S/S{s:03d}R{r:02d}.npy")
    alphas = np.load(f"alphas.npy")
    data[:,:,:,:,23] = alphas[:,:,:,::5]

    for kk, channel in enumerate(DEMO_CHANNELS):
        for i in range(24):
            dt = data[:,:,kk,:,i]
            min_i = dt.min()
            max_i = dt.max()
            scale = max_i - min_i
            if scale == 0:
                scale = 1
            dt -= min_i
            dt /= scale

    chans = np.zeros((4,), dtype=bool)
    ids = np.zeros((24,), dtype=bool)
    chans[[0,1,2,3]] = True
    # ids[6:] = True
    ids[[21, 22]] = True
    # ids[[21,22,23]] = True
    # ids = list(range(24))
    print(ids)
    data1 = data.mean(axis=3)
    print(data1.shape)
    data2  = np.zeros((0, (chans).sum() * (ids).sum()))
    for r in [1,2]:
        for s in range(1, Ss):
            dt = data1[s-1, r-1, chans][:,ids].flatten()[np.newaxis,:]
            data2 = np.concatenate((data2, dt), axis=0)
    print(data2.shape)
    for i in range(100):
        kmeans = KMeans(n_clusters=3, random_state=i)
        kmeans.fit(data2)
        # print(kmeans.inertia_)
        # print(kmeans.labels_[:109].sum()/109* 100, 100-kmeans.labels_[109:].sum()/109*100)
    plt.scatter(data2[:109,0], data2[:109,1])
    plt.scatter(data2[109:,0], data2[109:,1])
    plt.show()

if __name__ == "__main__":
    main()