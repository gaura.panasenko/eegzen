from tarfile import data_filter
import matplotlib.pyplot as plt
import matplotlib.colors as cls
import numpy as np
import json
import math
from scipy import signal
from numpy import linalg as LA
from sklearn.preprocessing import normalize
from scipy.ndimage import gaussian_filter, gaussian_filter1d

from common.params import ALL_CHANNELS, DEMO_CHANNELS, DEMO_S, SAMPLE_RATE, RQA_PARAMS, EPS
from common.rqa_analyse import calc_rp, calc_rqa
from teaspoon.parameter_selection.FNN_n import FNN_n
from teaspoon.parameter_selection.MI_delay import MI_for_delay
from sklearn.decomposition import FastICA

from common import filter_noise_min
from common.process_eeg import ProcessEEG
import warnings
warnings.simplefilter("ignore")


def main():
    plt.rcParams['figure.constrained_layout.use'] = True
    plt.rcParams['text.usetex'] = True
    plt.close('all')
    window = int(SAMPLE_RATE * 1.5)
    w = f"{window / SAMPLE_RATE:0.2f}"
    channels = [ALL_CHANNELS[i] for i in DEMO_CHANNELS]
    # dt = []

    j_names = ["Opened eyes", "Closed eyes"]

    pe = ProcessEEG(window, SAMPLE_RATE)


    count = 0
    arr = []
    out_plot = np.zeros((0,))

    for i in range(1,110):
        s = f"S{i:03d}"
        file = f"data/{s}R01.npy"
        data = np.load(file)
        data_ch = data[:SAMPLE_RATE * 20, channels[2]]
        f, t, Zxx = signal.stft(data_ch, SAMPLE_RATE, "hamming", window, window-1)
        # Pxx = pe.zxx_to_psd(Zxx)

        # print(Zxx.shape)
        noise_free = filter_noise_min(f)
        Zxx = np.apply_along_axis(lambda x: x * noise_free, 0, Zxx)
        _, out = signal.istft(Zxx, SAMPLE_RATE, "hamming", window, window-1)
        Zxx = np.abs(Zxx)
        # Zxx = gaussian_filter(Zxx, sigma=2)

        # freq_peak = np.argmax(Zxx.sum(axis=1)[f>=7])
        # count += (f[f>=7][freq_peak] < 15)
        # print(f[freq_peak], np.argmax(Zxx[freq_peak]))

        # ax[0].plot(f, Zxx[:,np.argmax(Zxx[freq_peak])])
        mean = Zxx.mean(axis=1)
        mean_max = np.argmax(mean)
        # mean = gaussian_filter1d(mean, sigma=1)
        xx = mean[(f>=7)&(f<=14)]
        xxf = f[(f>=7)&(f<=14)]
        # print(mean[(f>=8)&(f<=12)].mean()/mean.mean())
        # print(f[mean_max] > 6 and f[mean_max] < 14)
        count += f[mean_max] > 6 and f[mean_max] < 14
        if (f[mean_max] > 6 and f[mean_max] < 14):
            arr.append(i)
        xx -= xx.min()
        xx /= xx.max()
        # print((xx*xxf).sum()/xx.sum())
        xx = gaussian_filter1d(xx, sigma=2, mode="nearest")
        peaks, _ = signal.find_peaks(xx)
        gauss_mean = gaussian_filter1d(mean, sigma=2)
        gauss_mean /= gauss_mean.max()
        mean_peaks, _ = signal.find_peaks(gauss_mean, height=0.9)
        if mean_peaks.shape[0] > 0 and mean_peaks.shape[0] < 4:
            for j in mean_peaks:
                if f[j] > 8 and f[j] < 12:
                    print(f[mean_peaks], gauss_mean[mean_peaks])
                    # count += 1
                    # arr.append(i)
                    break
        # if not (f[mean_max] > 6 and f[mean_max] < 14) and peaks.shape[0] > 0:
        #     freq = xxf[peaks[0]]
            # print(freq)
            # count += 1
            # fig, ax = plt.subplots(1, 1)
            # ax.plot(f, mean)
            # print(freq,"+", i)
            # arr.append(freq)
        # if peaks.shape[0] == 0:
        #     arr.append(i)
        #     out_plot = np.concatenate([out_plot,[0]*10,mean[(f>=7)&(f<=14)]])
        # fig, ax = plt.subplots(1, 1)
        # ax.plot(f, mean)
        # ax[0].plot(data_ch)
        # ax[0].plot(out)
        # pcm = ax[1].pcolormesh(t, f, Zxx)
        # fig.colorbar(pcm, ax=ax[1], pad=0)

        # scale = np.linalg.norm(Zxx.flatten())
        scale = Zxx.max()
        # print(scale, Zxx.max())

        # Zxx /= scale

        # tau = MI_for_delay(out)
        # _, m = FNN_n(out, tau)
        # print(tau, m)
        # ax.plot(f, Zxx.mean(axis=1))
        # calc_rp(Zxx, tau, m, 0, 1, SAMPLE_RATE * 20, 0.2, ax, untreshhold=True)
        # # ax[1].plot(out)
        # l = calc_rqa(out/(out.max()-out.min())*2, 4, 4, 0.2)["L"]
        # print(l)
        # arr.append(l)
        # count += l < 2.4

    # ax.plot(out_plot)
    print(count)
    print(arr)
    plt.show()


if __name__ == "__main__":
    main()