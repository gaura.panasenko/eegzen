#!/usr/bin/env python3=
import numpy as np
from scipy.integrate import odeint

tEnd = 10
tSize = 100000
dSize = 1000
startPoint = [0.1, 0.1, 0.1]

def sys(X, t=0) -> np.ndarray:
    y1, y2, y3 = X
    return np.array([
        # ~ 1000-3*x-1000*y**2+10*z**2,
        # ~ y+2*z+x*(y+z*4/3),
        # ~ -2*y+z+x*(-y*4/3+z)
        # ~ 3*x+x*(x-3)*(5*y**2-z**2)/(1+y**2+z**2),
        # ~ 2*y-14*z-5*(x-3)*y,
        # ~ 14*y+2*z+5*(x-3)*z
        # ~ -3*x+140*y**2-z**2,
        # ~ y-200*z-140*x*y,
        # ~ 200*y+z+x*z
        2*y1-20*y3+3*y1**2-2*y2**2-y3**2-2*y2*y3-2*y1*y3,
        -0.5*y2+4*y2**2+8*y1*y2+4*y2*y3+4*y1*y3,
        20*y1+2*y3+4*y1*y3+2*y2*y3+y3**2
    ])

def main():
    x = odeint(sys, startPoint, np.linspace(0, tEnd, dSize), rtol=0.0000000001, atol=0.0000000001)
    np.save("some_sys_mini", x)

if __name__ == "__main__":
    main()
