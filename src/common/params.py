EPS: float = 0.15
SAMPLE_RATE = 160

ALL_CHANNELS = {'Fc5': 0, 'Fc3': 1, 'Fc1': 2, 'Fcz': 3, 'Fc2': 4, 'Fc4': 5, 'Fc6': 6, 'C5': 7, 'C3': 8, 'C1': 9, 'Cz': 10, 'C2': 11, 'C4': 12, 'C6': 13, 'Cp5': 14, 'Cp3': 15, 'Cp1': 16, 'Cpz': 17, 'Cp2': 18, 'Cp4': 19, 'Cp6': 20, 'Fp1': 21, 'Fpz': 22, 'Fp2': 23, 'Af7': 24, 'Af3': 25, 'Afz': 26, 'Af4': 27, 'Af8': 28, 'F7': 29, 'F5': 30, 'F3': 31, 'F1': 32, 'Fz': 33, 'F2': 34, 'F4': 35, 'F6': 36, 'F8': 37, 'Ft7': 38, 'Ft8': 39, 'T7': 40, 'T8': 41, 'T9': 42, 'T10': 43, 'Tp7': 44, 'Tp8': 45, 'P7': 46, 'P5': 47, 'P3': 48, 'P1': 49, 'Pz': 50, 'P2': 51, 'P4': 52, 'P6': 53, 'P8': 54, 'Po7': 55, 'Po3': 56, 'Poz': 57, 'Po4': 58, 'Po8': 59, 'O1': 60, 'Oz': 61, 'O2': 62, 'Iz': 63}

DEMO_CHANNELS = ["Af7", "Af8", "O1", "O2"]
DEMO_CHANNELS2 = ["Af7", "Af8", "T9", "T10"]
DEMO_S: str = "S028"

RQA_PARAMS = {
    'tau': "Time delay (tau)", 
    'm': "Embedded dimension (m)", 
    'eps': "Epsilon (eps)", 
    'L_min': "Minimum diagonal line length (L_min)", 
    'V_min': "Minimum vertical line length (V_min)", 
    'W_min': "Minimum white vertical line length (W_min)", 
    'RR': "Recurrence rate (RR)", 
    'DET': "Determinism (DET)", 
    'L': "Average diagonal line length (L)", 
    'L_max': "Longest diagonal line length (L_max)", 
    'DIV': "Divergence (DIV)", 
    'L_entr': "Entropy diagonal lines (L_entr)", 
    'LAM': "Laminarity (LAM)", 
    'TT': "Trapping time (TT)", 
    'V_max': "Longest vertical line length (V_max)", 
    'V_entr': "Entropy vertical lines (V_entr)", 
    'W': "Average white vertical line length (W)", 
    'W_max': "Longest white vertical line length (W_max)", 
    'W_div': "Longest white vertical line length divergence (W_div)", 
    'W_entr': "Entropy white vertical lines (W_entr)", 
    'DET/RR': "Ratio determinism / recurrence rate (DET/RR)", 
    'LAM/DET': "Ratio laminarity / determinism (LAM/DET)"
}

RQA_ORDER = ('tau', 'm', 'eps', 'nseg', 'L_min', 'V_min', 'W_min', 'RR', 'DET', 'L', 'L_max', 'DIV', 'L_entr', 'LAM', 'TT', 'V_max', 'V_entr', 'W', 'W_max', 'W_div', 'W_entr', 'DET/RR', 'LAM/DET')

RQA_IMPORTANT = [7, 8, 9,13, 14, 21, 22]

RQA_AVGS = {'tau': [3.96, 3.96, 3.96, 3.96], 'm': [2.97, 2.97, 2.97, 2.97], 'L_min': [1585.45, 1585.45, 1585.45, 1585.45], 'V_min': [1.98, 1.98, 1.98, 1.98], 'W_min': [1.98, 1.98, 1.98, 1.98], 'RR': [1.98, 1.98, 1.98, 1.98], 'DET': [0.22, 0.22, 0.14, 0.14], 'L': [0.68, 0.67, 0.68, 0.69], 'L_max': [4.2, 4.16, 3.25, 3.26], 'DIV': [164.37, 164.87, 112.18, 113.55], 'L_entr': [0.02, 0.02, 0.02, 0.02], 'LAM': [1.4, 1.37, 1.33, 1.32], 'TT': [0.78, 0.78, 0.8, 0.81], 'V_max': [5.35, 5.28, 3.63, 3.63], 'V_entr': [71.0, 72.25, 41.86, 41.87], 'W': [1.8, 1.76, 1.64, 1.64], 'W_max': [15.3, 15.18, 17.48, 17.54], 'W_div': [1383.58, 1378.18, 1375.06, 1362.56], 'W_entr': [0.0, 0.0, 0.0, 0.0], 'DET/RR': [3.13, 3.13, 3.47, 3.47], 'LAM/DET': [3.43, 3.46, 4.8, 4.82]}

FFT_AVGS = [26.14, 26.42, 40.50, 39.77]