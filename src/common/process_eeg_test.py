import numpy as np
from process_eeg import ProcessEEG
from params import ALL_CHANNELS, DEMO_S, DEMO_CHANNELS, SAMPLE_RATE

FILE = f"data/{DEMO_S}R03.npy"

def main():
    sig_list = [ALL_CHANNELS[i] for i in DEMO_CHANNELS]
    data = np.load(FILE)
    pe = ProcessEEG(SAMPLE_RATE * 2, SAMPLE_RATE)

    for i in sig_list:
        in_data = data[:,i]
        f, t, Zxx = pe.stft(in_data)
        t, out_data = pe.istft(Zxx)
        assert np.allclose(in_data, out_data[:in_data.shape[0]])

    for i in sig_list:
        in_data = data[:,i]
        f, t, Zxx = pe.stft(in_data, False)
        t, out_data = pe.istft(Zxx, False)
        min_size = min(in_data.shape[0], out_data.shape[0]) - SAMPLE_RATE
        assert np.allclose(in_data[SAMPLE_RATE:min_size], out_data[SAMPLE_RATE:min_size])

    f, t, Zxx = pe.stft(data[:,0])
    assert (pe.apply_filter(Zxx, pe.bandpass_filter())[0] == 0).all()
    

if __name__ == "__main__":
    main()
