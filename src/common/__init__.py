from functools import cache

import numpy as np
from scipy import signal

from process_eeg import bandpass_freq, lowpass_freq

@cache
def filter_noise_min(f):
    return 1-lowpass_freq(f, 0, 2)-bandpass_freq(f, 59, 61, 2)

@cache
def filter_noise_max(f):
    return bandpass_freq(f, 5, 48, 3)

def filter_noise_stft(x, rate, nperseg):
    f, t, Zxx = signal.stft(x, rate, nperseg=nperseg)
    noise_free = filter_noise_min(f)
    Zxx = np.apply_along_axis(lambda x: x * noise_free, 0, Zxx)
    return signal.istft(Zxx, rate, nperseg=nperseg)[1]

def normalize(x):
    """Apply Z-score normalization."""
    x_mean = x.mean()
    x_std = np.std(x)
    x = (x - x_mean) / x_std
    return x
