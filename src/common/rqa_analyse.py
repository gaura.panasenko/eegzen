#!/usr/bin/env python3
from collections import defaultdict
import matplotlib.pyplot as plt
import numpy as np
import time

from scipy.signal import argrelmin
from nolitsa import delay, noise, dimension

from teaspoon.parameter_selection.FNN_n import FNN_n
from teaspoon.parameter_selection.autocorrelation import autoCorrelation_tau
from teaspoon.parameter_selection.MI_delay import MI_for_delay

from pyrqa.time_series import TimeSeries
from pyrqa.time_series import EmbeddedSeries
from pyrqa.settings import Settings
from pyrqa.analysis_type import Classic, Cross
from pyrqa.neighbourhood import FixedRadius, Unthresholded
from pyrqa.metric import EuclideanMetric
from pyrqa.computation import RQAComputation
from pyrqa.computation import RPComputation
# from traitlets import default

EPS = 50
TAU = 1
M = 9
TAU_CALC = False
M_CALC = False
CRP = False
RQA = True
CHANNEL = 0
CRP_START = 0
#FILE = "some_sys_mini.npy"
FILE = "data/S007R01.npy"

RQA_ORDER = ('L_min', 'V_min', 'W_min', 'RR', 'DET', 'L', 'L_max', 'DIV', 'L_entr', 'LAM', 'TT', 'V_max', 'V_entr', 'W', 'W_max', 'W_div', 'W_entr', 'DET/RR', 'LAM/DET')


def calc_delay(x, maxtau=250, title="Finding minimal time delay"):
    mutual = delay.dmi(x, maxtau=maxtau)
    sma = noise.sma(mutual, hwin=1)
    i_delay = argrelmin(sma)[0] + 1

    # autocoleration = delay.acorr(x, maxtau=maxtau)
    # r_delay = np.argmax(autocoleration < 1.0 / np.e)

    fig, ax = plt.subplots(1, 1, sharex=True, layout='constrained', num=title)

    ax.set_title(title)
    ax.set_ylabel("Mutual information")
    ax.plot(mutual, 'k--', label="Raw")
    ax.plot(sma, 'k-', label="SMA")
    ax.scatter(i_delay, sma[i_delay], None, "k")
    text_dict = {
        "xytext": (-5, 10),
        "textcoords": 'offset points',
        "bbox": dict(boxstyle="round", fc=(1.0, 1.0, 1.0), ec=(.5, .5, .5))
    }
    for i in reversed(i_delay):
        ax.annotate(i, xy=(i, sma[i]), **text_dict)
    ax.legend()

    # ax[1].set_title("Autocorelation method")
    # ax[1].set_xlabel("Time delay (tau)")
    # ax[1].axhline(y = 1.0 / np.e, color = 'gray', linestyle = '--') 
    # ax[1].plot(autocoleration, 'k-')
    # ax[1].scatter(r_delay, autocoleration[r_delay], None, "k")
    # ax[1].annotate(r_delay, xy=(r_delay, autocoleration[r_delay]), **text_dict)

    return i_delay


def calc_dimension(x, tau, maxdim=8, title="Finding dimension", plotting = False):
    f, m = FNN_n(x, tau, plotting = False)
    f = np.concatenate(([np.nan], f, [0]))
    if plotting:
        plt.figure(title)
        plt.title(title)
        plt.xlabel("Embedded dimension (m)")
        plt.ylabel("FNN (%)")
        plt.plot(f, 'kx-')
        plt.gca().set_ylim((0, 100))
    return m

def calc_dimension_old(x, tau, maxdim=8, title="Finding dimension"):
    dim = np.arange(1, maxdim+1)
    f = dimension.fnn(x, tau=tau, dim=dim, R=10, A=2, metric='euclidean', maxnum=(10*tau+3))

    fig, ax = plt.subplots(1, 1, sharex=True, layout='constrained', num=title)
    ax.set_title(title)
    ax.set_xlabel("Embedded dimension (m)")
    ax.set_ylabel("FNN (%)")
    ax.axhline(y = 0.1, color = 'gray', linestyle = '--') 
    ax.plot(dim, f[0], 'k--', dim, f[1], 'k-.', dim, f[2], 'k-')
    ax.legend(["Test I", "Test II", "Test I + II"])

    if any(f[2] <= 0.1):
        return dim[f[2] <= 0.1][0]
    if any(f[0] <= 0.1):
        return dim[f[0] <= 0.1][0]
    if any(f[1] <= 0.1):
        return dim[f[1] <= 0.1][0]
    return 0

def calc_rp(x, tau, m, start, step=1, end=3000, eps=0.1, plotting=None, title="Recurrence plot", untreshhold=False):
    rp_start = start
    rp_x = x[start:end:step]
    rp_end = rp_x.shape[0] + rp_start
    if x.ndim == 1:
        x_final = TimeSeries(rp_x, embedding_dimension=m, time_delay=tau)
    else:
        x_final = EmbeddedSeries(rp_x)
    settings = Settings(x_final,
                        analysis_type=Classic,
                        neighbourhood=(FixedRadius(eps) if not untreshhold else Unthresholded()),
                        similarity_measure=EuclideanMetric,
                        theiler_corrector=1)
    computation = RPComputation.create(settings)
    result = computation.run()

    img = result.recurrence_matrix_reverse_normalized

    if plotting:
        # plotting.figure(title)
        plotting.imshow(img.copy(), cmap='Greys', interpolation='none', extent=[rp_start, rp_end] * 2)
        # plotting.set_title(title)

    return img


def calc_rqa_windowed(x, tau, m, win=240):
    assert x.ndim == 1
    wins = (x.shape[0] - win) // (win // 2)
    assert wins > 1
    out = defaultdict(float)
    for i in range(wins):
        x_i = x[i*win//2:i*win//2+win]
        xx, yy = np.meshgrid(x_i, x_i)
        eps = np.abs(xx - yy).mean()
        res = calc_rqa(x_i, tau, m, eps)
        for key in res.keys():
            out[key] += res[key] / wins
    return out

def calc_rqa_tuple(x, tau, m, eps=0.1):
    if x.ndim == 1:
        time_series = TimeSeries(x, embedding_dimension=m, time_delay=tau)
    else:
        time_series = EmbeddedSeries(x)
    settings = Settings(time_series,
                        analysis_type=Classic,
                        neighbourhood=FixedRadius(eps),
                        similarity_measure=EuclideanMetric,
                        theiler_corrector=1)
    computation = RQAComputation.create(settings)
    result = computation.run()
    return (
        float(result.min_diagonal_line_length),
        float(result.min_vertical_line_length),
        float(result.min_white_vertical_line_length),
        float(result.recurrence_rate),
        float(result.determinism),
        float(result.average_diagonal_line),
        float(result.longest_diagonal_line),
        float(result.divergence),
        float(result.entropy_diagonal_lines),
        float(result.laminarity),
        float(result.trapping_time),
        float(result.longest_vertical_line),
        float(result.entropy_vertical_lines),
        float(result.average_white_vertical_line),
        float(result.longest_white_vertical_line),
        float(result.longest_white_vertical_line_inverse),
        float(result.entropy_white_vertical_lines),
        float(result.ratio_determinism_recurrence_rate),
        float(result.ratio_laminarity_determinism),
    )

def calc_rqa(x, tau, m, eps=0.1):
    return dict(zip(RQA_ORDER, calc_rqa_tuple(x, tau, m, eps)))

def main() -> None:
    plt.rcParams['figure.constrained_layout.use'] = True

    print("Loading data...")
    start = time.time()
    x: np.ndarray = np.load(FILE)[:,CHANNEL]
    end = time.time()
    print(f"Took {end-start:.3} sec, {x.shape[0]} elements")

    filename = FILE.replace(".npy", "")

    tau = TAU
    if tau is None or TAU_CALC:
        print("Calculating tau...")
        start = time.time()
        i_delay = calc_delay(x, title=f"{filename} Time delay")
        #i_delay = MI_for_delay(x, plotting=TAU_CALC)
        if TAU_CALC:
            plt.figure()
        print(f'Minima of delayed mutual information = {i_delay}')
        end = time.time()
        if tau is None:
            tau = i_delay[0]
        print(f"Took {end-start:.3} sec, tau = {tau}")

    m = M
    if m is None or M_CALC:
        print("Calculating dimension...")
        start = time.time()
        mm = calc_dimension(x, tau, title=f"{filename} Embedded dimension tau={tau}", plotting=M_CALC)
        if m is None:
            m = mm
        end = time.time()
        print(f"Took {end-start:.3} sec, m = {m}")

    if CRP:
        print("Calculating cross recurrence plot...")
        start = time.time()
        title = f"{filename} Cross recurrence plot tau={tau} m={m} eps={EPS}"
        calc_rp(x, tau, m, CRP_START, eps=EPS, plotting=True, title=title)
        end = time.time()
        print(f"Took {end-start:.3} sec")

    if RQA:
        print("RQAComputation...")
        start = time.time()
        res = calc_rqa(x, tau, m, EPS)
        end = time.time()
        print(f"Took {end-start:.3} sec\n")
        print(res)

    plt.show()

if __name__ == "__main__":
    main()
