from typing import Any, TypeVar
import numpy as np
from numpy.fft import rfft, irfft, rfftfreq
import numpy.typing as npt
from sklearn.decomposition import FastICA

T = TypeVar("T", bound=npt.NBitBase)
N = TypeVar("N", bound=np.generic)
M = TypeVar("M", bound=np.generic)

def hann_func(x: npt.NDArray[np.floating[T]], df: float) -> npt.NDArray[np.floating[T]]:
    return 0.5 - 0.5 * np.cos(x * 2 * np.pi / (2 * df))


def highpass_freq(freq: npt.NDArray[np.floating[T]], x: float, df: float) -> npt.NDArray[np.floating[T]]:
    return hann_func(np.clip(freq - x + df, 0, df), df)


def lowpass_freq(freq: npt.NDArray[np.floating[T]], x: float, df: float) -> npt.NDArray[np.floating[T]]:
    return 1.0 - hann_func(np.clip(freq - x, 0, df), df)


def bandpass_freq(freq: npt.NDArray[np.floating[T]], low: float, high: float, df: float) -> npt.NDArray[np.floating[T]]:
    return highpass_freq(freq, low, df) * lowpass_freq(freq, high, df)


def bandpass_freq_smooth(freq: npt.NDArray[np.floating[T]], low: float, high: float, df: float) -> npt.NDArray[np.floating[T]]:
    return bandpass_freq(freq, low + df / 2, high - df / 2, df)

BANDS = ((1, 4), (4, 8), (7.5, 13), (13, 30), (30, 100), (60, 60))
# Bands: delta - 1-4, theta 4-8, alpha 8-12, beta 12-30, gamma 12-30,noise 60

class ProcessEEG:
    def __init__(self, win_size: int, rate: int):
        if win_size % 2 == 1:
            win_size += 1
        self.win_size: int = win_size
        self.part_size: int = win_size // 2
        window: npt.NDArray[np.float64] = np.hanning(win_size + 1)[:win_size]
        self.window: npt.NDArray[np.float64] = window
        self.scale: float = 2.0 / (rate * (window * window).sum())
        freqs: npt.NDArray[np.float64] = rfftfreq(win_size, 1 / rate)
        self.freqs: npt.NDArray[np.float64] = freqs
        self.rate: int = rate
        self._bandpass: np.ndarray[tuple[Any, Any], np.dtype[np.float64]] = np.array([
            bandpass_freq_smooth(freqs, left, right, 1) for left, right in BANDS
        ])
        self._bandpass -= bandpass_freq_smooth(freqs, 49.5, 50.5, 1)
        self._bandpass -= bandpass_freq_smooth(freqs, 59.5, 60.5, 1)

    def stft(self, data: npt.NDArray[np.generic], full=True) -> tuple[
        np.ndarray[tuple[N,], np.dtype[np.float64]],
        np.ndarray[tuple[M,], np.dtype[np.float64]],
        np.ndarray[tuple[N, M], np.dtype[np.complex128]]
    ]:
        part_size = self.part_size
        win_size = self.win_size
        fragments = max(data.shape[0] // self.part_size - 1, 0)
        frags = fragments + 3 if full else fragments
        t = np.arange(frags, dtype=np.float64) * self.part_size / self.rate

        Zxx: npt.NDArray[np.complex128] = np.zeros((self.freqs.shape[0], t.shape[0]), dtype=np.complex128)

        for i in range(fragments):
            start = i * part_size
            in_data = data[start : start + win_size]
            Zxx[:, i] = rfft(in_data * self.window, win_size)

        if full:
            Zxx[:, 1:fragments+1] = Zxx[:, :fragments]

            in_data = np.zeros((win_size,))
            in_data[part_size:] = data[:part_size]
            Zxx[:, 0] = rfft(in_data * self.window, win_size)

            for i in range(fragments, fragments + 2):
                in_data = np.zeros((win_size,))
                last_size = data.shape[0] - i * part_size
                in_data[:last_size] = data[i * part_size:]
                Zxx[:, i+1] = rfft(in_data * self.window, win_size)

        return self.freqs, t, Zxx
    
    def istft(self, Zxx: np.ndarray[tuple[N, M], np.dtype[np.complex128]], full=True):
        part_size = self.part_size
        win_size = self.win_size
        fragments = Zxx.shape[1]
        frags = fragments - 1 if full else fragments
        t: npt.NDArray[np.float64] = np.arange(frags * part_size, dtype=np.float64) / self.rate

        data: npt.NDArray[np.float64] = np.zeros(((fragments + 1) * part_size,), dtype=np.float64)

        for i in range(fragments):
            start = i * part_size
            data[start : start + win_size] += irfft(Zxx[:,i], win_size)

        if full:
            return t, data[part_size:-part_size]
        return t, data
    
    def bandpass_filter(self):
        data_pass = bandpass_freq_smooth(self.freqs, 1.5, 100, 1)
        data_pass -= bandpass_freq_smooth(self.freqs, 49.5, 50.5, 1)
        data_pass -= bandpass_freq_smooth(self.freqs, 59.5, 60.5, 1)
        return data_pass
    
    @staticmethod
    def apply_filter(Zxx: np.ndarray[tuple[N, M], np.dtype[np.complex128]], filt) \
            -> np.ndarray[tuple[N, M], np.dtype[np.complex128]]:
        return np.apply_along_axis(lambda x: x * filt, 0, Zxx)

    def filter_noise(self, data):
        f, t, Zxx = self.stft(data, True)
        self.apply_filter(Zxx, self.bandpass_filter())
        return self.istft(Zxx, True)
    
    def zxx_to_psd(self, Zxx):
        return np.abs(Zxx) ** 2 * self.scale
    
    def process_bands(self, Zxx):
        Pxx = self.zxx_to_psd(Zxx)
        Psd_bands = np.array([self.apply_filter(Pxx, i) for i in self._bandpass])
        bands = Psd_bands.sum(axis=1)
        return bands
    
    def process_full(self, data):
        data_filt = np.zeros(data.shape)
        for i in range(data.shape[1]):
            data_filt[:, i] = self.filter_noise(data[:, i])[1][:data.shape[0]]
        ica = FastICA(n_components=4, random_state=0, max_iter=1000)
        transformed_data = ica.fit_transform(data_filt)

        max_Zxx = []
        Zxxs = []
        f, t, _ = self.stft(transformed_data[:, 0])
        for i in range(transformed_data.shape[1]):
            _, _, Zxx = self.stft(transformed_data[:, i])
            Zxxs.append(Zxx)
            max_Zxx.append(np.abs(Zxx).max())

        inval = np.array(max_Zxx).argmax()
        Zxxs[inval][:] = 0
        Zxxs = np.abs(Zxxs).sum(axis=0)

        bands = self.process_bands(Zxxs)
        bands_old = bands.copy()
        for i in range(bands.shape[1]):
            bands[:,i] = np.mean(bands_old[:,max(i-2, 0):i+2], axis=1)

        relax1 = bands[1:3, :].sum(axis=0) / bands[1:5, :].sum(axis=0)
        relax2 = bands[2, :] / bands[1:5, :].sum(axis=0)
        relax3 = bands[1:3, :].sum(axis=0) / bands[1:4, :].sum(axis=0)

        relax1 = bandpass_freq_low(relax1, 0.92, 0.7)
        relax2 = bandpass_freq_low(relax2, 0.6, 0.6)
        relax3 = bandpass_freq_low(relax3, 0.88, 0.6)

        return t, bands, (relax1, relax2, relax3)

    # def process(self, dt, reverse=False):
    #     win_size = self.win_size
    #     dt = dt[-win_size:]
    #     Zxx = rfft(dt * self.window, win_size)

    #     Zxx_bands = [Zxx * i for i in self._bandpass]

    #     data = []
    #     if reverse:
    #         data = [irfft(i, win_size) for i in Zxx_bands]
    #         data += [irfft(Zxx, win_size)]

    #     scale = self.scale
    #     bands = [np.sum(np.abs(i) ** 2 * scale) for i in Zxx_bands]

    #     relax = bands[2] / np.sum(bands[5])
    #     concetr = bands[3] / np.sum(bands[5])
    #     out = (relax, concetr)

    #     return out, bands, data

    # def process_all(self, data, reverse=False):
    #     fragments = max(data.shape[0] // self.part_size - 1, 0)
    #     win_size = self.win_size

    #     infos = np.zeros(
    #         (
    #             2,
    #             fragments,
    #         )
    #     )
    #     bands = np.zeros(
    #         (
    #             6,
    #             fragments,
    #         )
    #     )
    #     data_rev = np.zeros((7, data.shape[0]))

    #     for i in range(fragments):
    #         start = i * self.part_size
    #         dt = data[start : start + win_size]
    #         infos[:, i], bands[:, i], out_data = self.process(dt, reverse)
    #         if reverse:
    #             data_rev[:, start : start + win_size] += out_data

    #     # bands = 10*np.log10(bands+0.01)

    #     return fragments * self.part_size, infos, bands, data_rev


    def empty(self):
        infos = np.zeros(
            (
                2,
                1,
            )
        )
        bands = np.zeros(
            (
                5,
                1,
            )
        )
        data_rev = np.zeros((6, 1))
        return infos, bands, data_rev
