import time

import numpy as np
import matplotlib.pyplot as plt
from scipy.fft import fft, fftfreq

from brainflow.data_filter import DataFilter, FilterTypes, AggOperations, NoiseTypes


FILE = "data/S007R01.npy"

def main():
    raw_data = np.load(FILE)[:,0]
    data = np.zeros((7, raw_data.shape[0]))
    for i in range(7):
        data[i] = raw_data

    sampling_rate = 190
    nfft = DataFilter.get_nearest_power_of_two(sampling_rate)

    eeg_channels = range(1, 7)

    for count, channel in enumerate(eeg_channels):
        if count == 0:
            DataFilter.perform_bandpass(data[channel], sampling_rate, 2.0, 50.0, 4, FilterTypes.BESSEL_ZERO_PHASE, 0)
        elif count == 1:
            DataFilter.perform_bandstop(data[channel], sampling_rate, 48.0, 52.0, 3, FilterTypes.BUTTERWORTH_ZERO_PHASE, 0)
        elif count == 2:
            DataFilter.perform_lowpass(data[channel], sampling_rate, 50.0, 5, FilterTypes.CHEBYSHEV_TYPE_1_ZERO_PHASE, 1)
        elif count == 3:
            DataFilter.perform_highpass(data[channel], sampling_rate, 2.0, 4, FilterTypes.BUTTERWORTH, 0)
        elif count == 4:
            DataFilter.perform_rolling_filter(data[channel], 3, AggOperations.MEAN.value)
        else:
            DataFilter.remove_environmental_noise(data[channel], sampling_rate, NoiseTypes.FIFTY.value)
            print(count, channel)

    for i in range(7):
        win = 1024
        f = np.abs(fft(data[i,:win], win))[:win//2]
        plt.plot(fftfreq(win, 1/sampling_rate)[:win//2], f)
    plt.legend(["Raw", "bandpass", "bandstop", "lowpass", "highpass", "rolling_filter", "remove_environmental_noise"])
    plt.show()


if __name__ == "__main__":
    main()