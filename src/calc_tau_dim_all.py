#!/usr/bin/env python3
import matplotlib.pyplot as plt
import numpy as np
import json

from teaspoon.parameter_selection.FNN_n import FNN_n
from teaspoon.parameter_selection.autocorrelation import autoCorrelation_tau
from teaspoon.parameter_selection.MI_delay import MI_for_delay

from common.params import DEMO_CHANNELS, ALL_CHANNELS

def main():
    if input("Are you sure? (y/n)") != "y":
        return
    try:
        with open('params.json', 'r', encoding='utf-8') as f:
            data = json.load(f)
    except FileNotFoundError:
        data = {}
    try:
        for i in range(1, 110):
            i_name = f"S{i:03d}"
            for j in range(1, 4):
                j_name = f"R{j:02d}"
                name = f"{i_name}{j_name}"
                if name in data:
                    continue
                print(f"Processing {name}...")
                filename = f"data/{name}.npy"
                xx = np.load(filename)
                res_dict = {}
                for channel in DEMO_CHANNELS:
                    x = xx[:,ALL_CHANNELS[channel]]
                    mi = MI_for_delay(x)
                    ac = autoCorrelation_tau(x)
                    m = -1
                    try:
                        _, m = FNN_n(x, ac)
                    except ValueError:
                        pass
                    res_dict[channel] = {
                        "tau": mi,
                        "m": m
                    }
                data[name] = res_dict
    finally:
        with open('params.json', 'w', encoding='utf-8') as f:
            json.dump(data, f, ensure_ascii=False, indent=4)

if __name__ == "__main__":
    main()