from tarfile import data_filter
import matplotlib.pyplot as plt
import matplotlib.colors as cls
import numpy as np
import json
import math
from scipy import signal
from numpy import linalg as LA
from sklearn.preprocessing import normalize
from scipy.ndimage import gaussian_filter, gaussian_filter1d

from common.params import ALL_CHANNELS, DEMO_CHANNELS, DEMO_S, SAMPLE_RATE, RQA_PARAMS, EPS
from common.rqa_analyse import calc_rp, calc_rqa
from teaspoon.parameter_selection.FNN_n import FNN_n
from teaspoon.parameter_selection.MI_delay import MI_for_delay
from sklearn.decomposition import FastICA

from common import filter_noise_min
import warnings
warnings.simplefilter("ignore")


def main():
    plt.rcParams['figure.constrained_layout.use'] = True
    plt.rcParams['text.usetex'] = True
    plt.close('all')
    # window = int(SAMPLE_RATE * 1.5)
    window = 240
    w = f"{window / SAMPLE_RATE:0.2f}"
    channels = [ALL_CHANNELS[i] for i in DEMO_CHANNELS]
    # dt = []

    j_names = ["Opened eyes", "Closed eyes"]

    arr = [[],[]]

    for s in range(1,4):
        for r in range(1, 3):
            file = f"data/S{s:03d}R{r:02}.npy"
            data = np.load(file)
            data_ch = data[:SAMPLE_RATE * 5, channels[2]]
            f, t, Zxx = signal.stft(data_ch, SAMPLE_RATE, "hamming", window, window-1)

            noise_free = filter_noise_min(f)
            Zxx = np.apply_along_axis(lambda x: x * noise_free, 0, Zxx)
            _, out = signal.istft(Zxx, SAMPLE_RATE, "hamming", window, window-1)
            Zxx = np.abs(Zxx)
            # Zxx = gaussian_filter(Zxx, sigma=2)

            # scale = np.linalg.norm(Zxx.flatten())
            # scale = Zxx.max()
            # Zxx /= scale

            # tau = MI_for_delay(out)
            # _, m = FNN_n(out, tau)
            # print(tau, m)
            # ax.plot(f, Zxx.mean(axis=1))
            xx, yy = np.meshgrid(out, out)
            dists = np.abs(xx - yy).flatten()
            # print(xx.shape, yy.shape, np.mean(dists), min(dists), np.mean(out))
            eps = np.mean(dists)

            fig, ax = plt.subplots(1, 1, sharex=True, sharey=True)
            # ax.plot(dists)
            # tmp = np.sqrt((Zxx**2).sum(axis=0))
            # ax.plot(tmp)
            # print(tmp.shape)
            # eps = (tmp.mean()-tmp.min()) * 0.2 + tmp.min()

            mat = calc_rp(out, 4, 4, 0, 1, SAMPLE_RATE * 20, eps, ax, untreshhold=False)
            # img = (mat/mat.max()*255).astype(np.int64)
            # histogram = np.array([(img == i).sum() for i in range(256)])
            # threshold = int(np.argmax(histogram) * 0.9)
            # img[img >= threshold] = 255
            # img[img < threshold] = 0
            # ax.plot(histogram)
            # ax.imshow(255-img, cmap='Greys', interpolation='none')

            # fig.set_size_inches(30//2,18//2)
            # fig.savefig(f"figs/S{i:03d}R02_stft_crp.png")

            # calc_rp(Zxx.transpose(), 4, 4, 0, 1, SAMPLE_RATE * 20, 15.84, ax, untreshhold=True)
            # # ax[1].plot(out)
            l = calc_rqa(out, 4, 4, eps)["L"]
            print(l)
            arr[r-1].append(l)
            # count += l < 2.4

    plt.plot(arr[0])
    plt.plot(arr[1])
    plt.show()


if __name__ == "__main__":
    main()