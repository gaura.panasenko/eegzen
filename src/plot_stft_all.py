#!/usr/bin/env python3
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.colors as colors
from scipy.signal import stft

from common.params import EPS, ALL_CHANNELS, DEMO_CHANNELS, DEMO_S, SAMPLE_RATE

def main():
    plt.rcParams['figure.constrained_layout.use'] = True
    plt.close('all')

    sig_list = [ALL_CHANNELS[i] for i in DEMO_CHANNELS]

    for s in range(43, 44):
        file = f"data/S{s:03d}R01.npy"
        all_data = np.load(file)
        data_opened = all_data[:, sig_list]

        file = f"data/S{s:03d}R02.npy"
        all_data = np.load(file)
        data_closed = all_data[:, sig_list]

        length = min(data_closed.shape[0], data_opened.shape[0])
        data_closed = data_closed[:length,:]
        data_opened = data_opened[:length,:]
        data_eye = [data_opened, data_closed]

        fig, ax = plt.subplots(2, 2, sharex=True, sharey=True)
        fig.suptitle(f"S{s:03d} - STFT Magnitude")
        x = np.arange(length) / SAMPLE_RATE
        ax[1,0].set_xlabel("Opened eyes, Time, sec")
        ax[1,1].set_xlabel("Closed eyes, Time, sec")
        for i in range(2):
            for j in range(2):
                dt = data_eye[j][:, i+2]
                ax[i,j].set_ylabel(f"{DEMO_CHANNELS[i+2]}, Frequency, Hz")
                f, t, Zxx = stft(dt, SAMPLE_RATE, nperseg=SAMPLE_RATE*10)
                pcm = ax[i,j].pcolormesh(t, f, np.abs(Zxx), norm=colors.LogNorm(vmax=155))
                fig.colorbar(pcm, ax=ax[i,j], pad=0)
        fig.set_size_inches(30//2,18//2)
        fig.savefig(f"figs/S{s:03d}_STFT_long.png")
        plt.close()
    # plt.show()

if __name__ == "__main__":
    main()
