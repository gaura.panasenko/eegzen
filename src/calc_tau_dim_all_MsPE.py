#!/usr/bin/env python3
import matplotlib.pyplot as plt
import numpy as np
import json

from teaspoon.parameter_selection.MsPE import MsPE_n,  MsPE_tau

signames = ["Fp1", "Fp2", "Af7"]
m_s, m_e, d_s, d_e = 3, 7, 1, 200

def main():
    if input("Are you sure? (y/n)") != "y":
        return
    try:
        with open('params_MsPE.json', 'r', encoding='utf-8') as f:
            data = json.load(f)
    except FileNotFoundError:
        data = {}
    try:
        for i in range(1, 3):
            i_name = f"S{i:03d}"
            for j in range(1, 4):
                j_name = f"R{j:02d}"
                name = f"{i_name}{j_name}"
                if name in data:
                    continue
                print(f"Processing {name}...")
                filename = f"data/{name}.npy"
                xx = np.load(filename)
                res_dict = {}
                for k in range(3):
                    x = xx[:,k]
                    tau = int(MsPE_tau(x, d_e, plotting = False))
                    if tau > 0:
                        m = int(MsPE_n(x, tau, m_s, m_e, plotting = False))
                    else:
                        m = 0
                    res_dict[signames[k]] = {
                        "tau": tau,
                        "m": m
                    }
                    print(f"{name} {signames[k]} {tau} {m}")
                data[name] = res_dict
    finally:
        with open('params_MsPE.json', 'w', encoding='utf-8') as f:
            json.dump(data, f, ensure_ascii=False, indent=4)

if __name__ == "__main__":
    main()