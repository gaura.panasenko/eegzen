import matplotlib.pyplot as plt
import numpy as np
import json
import math

from common.params import ALL_CHANNELS, DEMO_CHANNELS, DEMO_S, SAMPLE_RATE, RQA_PARAMS

def main():
    plt.rcParams['figure.constrained_layout.use'] = True
    plt.close('all')

    with open("rqa/rqa.json", "r") as f:
        d = json.load(f)

    Ss = 110

    for l, (param, descr) in enumerate(RQA_PARAMS.items()):
        if param == "eps":
            continue
        t = f"{param}"
        for kk, channel in enumerate(DEMO_CHANNELS):
            k = ALL_CHANNELS[channel]
            out_data = np.zeros((Ss, 2))
            for i in range(1, Ss+1):
                for j in range(1, 3):
                    key_s = f"S{i:03d}R01_{channel}"
                    key = f"S{i:03d}R{j:02d}_{channel}"
                    if key in d and key_s in d:
                        val = d[key][param]
                        out_data[i-1, j-1] = val
            mm = out_data.mean()
            m1 = out_data[:,0].mean()
            m2 = out_data[:,1].mean()
            if m1 < m2:
                good_r1 = out_data[:,0] < mm
                good_r2 = out_data[:,1] > mm
            else:
                good_r1 = out_data[:,0] > mm
                good_r2 = out_data[:,1] < mm
            diff_data = out_data[:,0] - out_data[:,1]
            diff_data_mean = diff_data.mean()
            diff_data_mean_perc = round((diff_data <= -0.5).sum() / Ss * 100, 2)
            perc1 = round(good_r1.sum() / Ss * 100, 2)
            perc2 = round(good_r2.sum() / Ss * 100, 2)
            perc3 = round(((good_r1 & good_r2).sum()) / Ss * 100, 2)
            # perc2 = round(((out_data[:,0]>mm)&(out_data[:,1]<mm)).sum()/Ss * 100, 2)
            if (perc1 > 55 and perc2 > 55):
                print(f"{t:10s}|{channel:5s}|{perc1:10.2f}%|{perc2:10.2f}%|{perc3:10.2f}%|{mm:10.2f}| {diff_data_mean:10.2f}|{diff_data_mean_perc:10.2f}%")
                # print(np.arange(1, Ss+1)[(good_r1 & good_r2)])

if __name__ == "__main__":
    main()