import numpy as np
from scipy import signal
from scipy.signal import welch
from numpy.fft import rfft, rfftfreq

from common.params import ALL_CHANNELS, DEMO_CHANNELS, DEMO_S, SAMPLE_RATE, RQA_PARAMS, EPS
from common import filter_noise_min
from common.process_eeg import bandpass_freq_smooth

nseg = int(SAMPLE_RATE * 5) * 2
f = rfftfreq(nseg, 1 / SAMPLE_RATE)
noise_free = filter_noise_min(f)
delta_pass = bandpass_freq_smooth(f, 0, 4, 2)
theta_pass = bandpass_freq_smooth(f, 4, 8, 2)
low_alpha_pass = bandpass_freq_smooth(f, 8, 10, 2)
high_alpha_pass = bandpass_freq_smooth(f, 10, 12, 2)
low_beta_pass = bandpass_freq_smooth(f, 12, 16, 2)
mid_beta_pass = bandpass_freq_smooth(f, 16, 20, 2)
high_beta_pass = bandpass_freq_smooth(f, 20, 30, 2)
low_gamma_pass = bandpass_freq_smooth(f, 30, 40, 2)
mid_gamma_pass = bandpass_freq_smooth(f, 40, 50, 2)
high_gamma_pass = bandpass_freq_smooth(f, 30, 1000, 2)

def calc(filename):
    x = np.load(filename)
    nsegs = (x.shape[0] - nseg) // (nseg // 10)
    out = np.zeros((64, nsegs, 10))

    # for c, channel in enumerate(DEMO_CHANNELS):
    #     e = ALL_CHANNELS[channel]
    for e in range(64):
        c = e

        for i in range(nsegs):
            x_i = x[i*nseg//10:i*nseg//10+nseg, e]
            #Zxx = np.abs(rfft(x_i))**2 / (nseg**2) * 2 * (nseg/SAMPLE_RATE)
            Zxx = np.abs(rfft(x_i)) / (nseg)
            out[c, i, 0] = (Zxx * delta_pass).sum()
            out[c, i, 1] = (Zxx * theta_pass).sum()
            out[c, i, 2] = (Zxx * low_alpha_pass).sum()
            out[c, i, 3] = (Zxx * high_alpha_pass).sum()
            out[c, i, 4] = (Zxx * low_beta_pass).sum()
            out[c, i, 5] = (Zxx * mid_beta_pass).sum()
            out[c, i, 6] = (Zxx * high_beta_pass).sum()
            out[c, i, 7] = (Zxx * low_gamma_pass).sum()
            out[c, i, 8] = (Zxx * mid_gamma_pass).sum()
            out[c, i, 9] = (Zxx * high_gamma_pass).sum()
    return out


def main():
    if input("Are you sure? (y/n)") != "y":
        return
    data = np.zeros((109, 2, 64, 50, 10))
    for i in range(1, 110):
        i_name = f"S{i:03d}"
        # for j in range(3, 5):
        #     j_name = f"R{j:02d}"
        #     for k in range(3):
        #         name = f"{i_name}{j_name}T{k}"
        #         print(f"Processing {name}...")
        #         filename = f"data/T/{name}.npy"
        #         calc(name, filename, data)
        for j in range(1, 3):
            j_name = f"R{j:02d}"
            name = f"{i_name}{j_name}"
            print(f"Processing {name}...")
            filename = f"data/{name}.npy"
            data[i-1, j-1] = calc(filename)[:,:50,:]
    np.save("bands.npy", data)


if __name__ == "__main__":
    main()