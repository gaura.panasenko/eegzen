import matplotlib.pyplot as plt
import numpy as np
import json
import math
from scipy import signal
from numpy import linalg as LA

from common.params import ALL_CHANNELS, DEMO_CHANNELS, DEMO_S, SAMPLE_RATE, RQA_PARAMS, EPS
from common.rqa_analyse import calc_rp, calc_rqa

def norm(x):
    return np.sqrt(np.mean(x**2)) * 4

def main():
    plt.rcParams['figure.constrained_layout.use'] = True
    plt.close('all')
    window = int(SAMPLE_RATE * 1.5)
    channels = [ALL_CHANNELS[i] for i in DEMO_CHANNELS]
    dt = []
    
    for j in range(1, 5):
        file = f"data/S015R{j:02d}.npy"
        data = np.load(file)
        dt.append(data[:, channels])

    dt_norm = norm(np.concatenate(dt))
    print(dt_norm)

    for j in dt:
        j /= dt_norm
    # data[:, channel] /= LA.norm(data[:, channel])
    # data[:, channel] = signal.detrend(data[:, channel])
    fig, ax = plt.subplots(1, 1)

    for j, data in enumerate(dt):
        size = data.shape[0]//window - 1
        for k in range(4):
            l = np.zeros((size, ))
            for i in range(size):
                start = i * window
                x = data[start:start + window,k]
                res = calc_rqa(x, 1, 4, EPS)
                l[i] = res["L"]
            ax.plot(l)
            print(j, k, np.mean(l), calc_rqa(data[:,k], 1, 4, EPS)["L"])

    fig, ax = plt.subplots(1, 1, sharex=True, sharey=True)
    for i in dt:
        ax.plot(i, alpha=0.7)

    plt.show()


if __name__ == "__main__":
    main()