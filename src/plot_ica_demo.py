#!/usr/bin/env python3
import numpy as np
import matplotlib.pyplot as plt
from sklearn.decomposition import FastICA
from picard import picard
# from eeg_positions import (
#     get_alias_mapping,
#     get_available_elec_names,
#     get_elec_coords,
# )


from common.params import EPS, ALL_CHANNELS, DEMO_CHANNELS2, DEMO_S, SAMPLE_RATE
import common.process_eeg
import importlib
importlib.reload(common.process_eeg)
from common.process_eeg import ProcessEEG

DEMO_S = "S033"
FILE = f"data/{DEMO_S}R03.npy"

def main():
    plt.rcParams['figure.constrained_layout.use'] = True

    # coords = get_elec_coords(
    #     elec_names=[i.upper() for i in ["Tp9", "Tp10"] + [j for j in ALL_CHANNELS.keys() if "z" not in j and j != "Fp1" and j != "Fp2"]],
    #     drop_landmarks=False,
    #     dim="3d",
    # )
    # coords = coords.to_numpy()
    # tp9 = coords[coords[:,0] == "TP9"]
    # coords[:,1:] -= tp9[0,1:]
    # coords = sorted(coords, key=lambda coord: np.sum(coord[1:] ** 2))
    # print(np.array(coords))
    # coords = get_elec_coords(
    #     elec_names=[i.upper() for i in DEMO_CHANNELS2],
    #     drop_landmarks=False,
    #     dim="3d",
    # )
    # coords = coords.to_numpy()
    # coords = np.concatenate((coords[:,1:], [[1]] * 4), axis=1)

    sig_list = [ALL_CHANNELS[i] for i in DEMO_CHANNELS2]

    all_data = np.load(FILE)
    data = all_data[:, sig_list]
    data_filt = np.zeros(data.shape)

    pe = ProcessEEG(SAMPLE_RATE, SAMPLE_RATE)
    for i in range(len(DEMO_CHANNELS2)):
        data_filt[:, i] = pe.filter_noise(data[:, i])[1][:data.shape[0]]

    K, W, transformed_data = picard(data_filt.transpose(), random_state=0)
    # Winv = np.linalg.inv(W)
    # new_coords = Winv.dot(coords)
    # div = new_coords[:,3].copy()
    # old_coords = W.dot(new_coords)
    # for i in range(4):
    #     new_coords[:,i] /= div
    #     old_coords[:,i] /= old_coords[:,3]
    # print(coords)
    # print()
    # print(new_coords)
    # print()
    # print(np.sum(new_coords[:,:3]**2, axis=1))
    # print()
    # print(old_coords)
    # print()
    transformed_data = transformed_data.transpose() / 2

    fig, ax = plt.subplots(4, 2, sharex=True)
    x = np.arange(data.shape[0]) / SAMPLE_RATE
    x_ids = (x >= 0) & (x <= 10)
    ax[3,0].set_xlabel("Raw signal")
    ax[3,1].set_xlabel("ICA")
    for i in range(4):
        ax[i,0].set_ylabel(DEMO_CHANNELS2[i])
        ax[i,0].plot(x[x_ids], data_filt[x_ids, i])
        ax[i,1].plot(x[x_ids], transformed_data[x_ids,i])
    fig.set_size_inches(30//2,18//2)
    fig.savefig("figs/ICA_demo.png")
    plt.show()

if __name__ == "__main__":
    main()
