import numpy as np
from sklearn.cluster import KMeans

from common.params import ALL_CHANNELS, DEMO_CHANNELS, DEMO_S, SAMPLE_RATE, RQA_PARAMS

def main():
    data = np.load(f"bands.npy")


    Ss = 109 + 1

    inv_channel = {v: k for k, v in ALL_CHANNELS.items()}
    for channel in DEMO_CHANNELS:
        kk = ALL_CHANNELS[channel]
        out_data = np.zeros((Ss * 50 * 2,10))
        for s in range(1, Ss):
            for r in (1, 2):
                kkk = (r-1) * Ss * 50 + (s-1) * 50
                dt = data[s-1, r-1, kk, :, :]
                #out_data[kkk:kkk+50,:] = dt
                for bk, bv in enumerate((2,3,4,6)):
                    out_data[kkk:kkk+50,bk] = dt[:,bv] / dt.sum(axis=1)
                #out_data[kkk:kkk+50,0] = dt[:,2:4].sum(axis=1)/dt.sum(axis=1)
                #out_data[kkk:kkk+50,1] = dt[:,4:6].sum(axis=1)/dt.sum(axis=1)
        
        print(f"Processing {channel}")
        init = (out_data[:Ss*50].mean(axis=0), out_data[Ss*50:].mean(axis=0), out_data[:].mean(axis=0))
        d1 = ((out_data[:Ss*50] - init[0])**2).sum(axis=1)
        d2 = ((out_data[:Ss*50] - init[1])**2).sum(axis=1)
        d12=(d1<d2).sum()/d1.shape[0]
        d3 = ((out_data[Ss*50:] - init[0])**2).sum(axis=1)
        d4 = ((out_data[Ss*50:] - init[1])**2).sum(axis=1)
        d34=(d3>d4).sum()/d1.shape[0]
        print(d12,d34,min(d12,d34))
        #kmeans = KMeans(n_clusters=3, init=init)
        #kmeans.fit(out_data)
        #print(kmeans.labels_[:1000:50], kmeans.labels_[-1000::50])


if __name__ == "__main__":
    main()