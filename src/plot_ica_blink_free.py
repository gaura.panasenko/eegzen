#!/usr/bin/env python3
from re import T
import numpy as np
import matplotlib.pyplot as plt
from sklearn.decomposition import FastICA
from scipy import signal
from picard import picard

from common.params import EPS, ALL_CHANNELS, DEMO_CHANNELS2, DEMO_S, SAMPLE_RATE
import common.process_eeg
import importlib
importlib.reload(common.process_eeg)
from common.process_eeg import ProcessEEG

DEMO_S = "S051"
FILE = f"data/{DEMO_S}R04.npy"

def main():
    plt.rcParams['figure.constrained_layout.use'] = True

    sig_list = [ALL_CHANNELS[i] for i in DEMO_CHANNELS2]

    all_data = np.load(FILE)
    data = all_data[:, sig_list]
    data_filt = np.zeros(data.shape)

    pe = ProcessEEG(SAMPLE_RATE, SAMPLE_RATE)
    for i in range(len(DEMO_CHANNELS2)):
        data_filt[:, i] = pe.filter_noise(data[:, i])[1][:data.shape[0]]

    ica = FastICA(n_components=4, random_state=0)
    transformed_data = ica.fit_transform(data_filt)  # Reconstruct signals
    A_ = ica.mixing_  # Get estimated mixing matrix

    # We can `prove` that the ICA model applies by reverting the unmixing.
    assert np.allclose(data_filt, np.dot(transformed_data, A_.T) + ica.mean_)

    max_Zxx = []
    Zxxs = []
    f, t, _ = pe.stft(transformed_data[:, 0])
    for i in range(transformed_data.shape[1]):
        _, _, Zxx = pe.stft(transformed_data[:, i])
        Zxxs.append(Zxx)
        max_Zxx.append(np.abs(Zxx).max())
    
    inval = np.array(max_Zxx).argmax()
    Zxxs[inval][:] = 0
    Zxxs = np.abs(Zxxs).sum(axis=0)
    Zxxs2 = pe.stft(data_filt.sum(axis=1))[2]
    # Zxxs = Zxxs[0]

    # tsig, final_sig = pe.istft(Zxxs)

    fig, ax = plt.subplots(4, 1, sharex=True)

    x_filt = lambda x: (x >= 5) & (x <= 55)
    t_ids = x_filt(t)
    # tsig_ids = x_filt(tsig)
    # x = np.arange(transformed_data.shape[0]) / SAMPLE_RATE
    # x_ids = x_filt(x)
    # x_win_scale = pe.part_size / SAMPLE_RATE

    bands = pe.process_bands(Zxxs)
    bandsx = bands.copy()
    for i in range(bands.shape[1]):
        bandsx[:,i] = np.mean(bands[:,max(i-2, 0):i+2], axis=1)
    bands2 = pe.process_bands(Zxxs2)
    tt, dt = pe.istft(Zxxs2)
    tt_ids = x_filt(tt)
    # xb = np.arange(bands.shape[1]) * x_win_scale
    # xb_ids = x_filt(xb)
    # ax[0].plot(t, np.abs(Zxxs).sum(axis=0))
    # ax[0].plot(t[t_ids], bands[2, t_ids] / bands[1:5,t_ids].sum(axis=0))
    # ax[0].plot(t[t_ids], bandsx[2, t_ids] / bandsx[1:5,t_ids].sum(axis=0))
    # ax[1].plot(tt[tt_ids], dt[tt_ids])
    for i in range(4):
        ax[i].plot(tt[tt_ids], transformed_data[tt_ids[:transformed_data.shape[0]],i] / 2)
    # ax[0].plot(t, bands[:5,:].sum(axis=0))
    # ax[0].plot(t, bands[3,:])
    # ax[0].plot(t, bands[2,:])
    # ax[1].plot(tsig[tsig_ids], final_sig[tsig_ids])

    # _, _, bands, _ = pe.process_all(data.sum(axis=1))
    # xb = np.arange(bands.shape[1]) * x_win_scale
    # xb_ids = x_filt(xb)
    # ax[0].plot(xb[xb_ids], bands[2,xb_ids] / bands[1:5,xb_ids].sum(axis=0))

    # ax[1].plot(x[x_ids], final_sig[x_ids])
    # ax[2].plot(x[x_ids], data.sum(axis=1)[x_ids])

    # ax[0].plot(x, final_sig)
    # ax[1].pcolormesh(t, f[f<50], np.abs(Zxx[f<50,:]), shading='gouraud')

    # for i in range(4):
        # ax[i].plot(t, np.abs(Zxx[2,:]))
        # ax[i].pcolormesh(t, f[f<20], np.abs(Zxx[f<20,:]), shading='gouraud')
        # ax[i].plot(x, transformed_data[:, i])
    fig.set_size_inches(30//2,18//2)
    fig.savefig("figs/ICA_demo.png")
    plt.show()

if __name__ == "__main__":
    main()
