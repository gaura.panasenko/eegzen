import matplotlib.pyplot as plt
import numpy as np

from common.params import ALL_CHANNELS, DEMO_CHANNELS, DEMO_S, SAMPLE_RATE, RQA_PARAMS

def main():
    plt.rcParams['figure.constrained_layout.use'] = True
    plt.close('all')

    data = np.load(f"alphas.npy")

    Ss = 109 + 1

    # for kk, channel in enumerate(DEMO_CHANNELS):
    #     k = ALL_CHANNELS[channel]
    inv_channel = {v: k for k, v in ALL_CHANNELS.items()}
    for kk in range(64):
        k = kk
        channel = inv_channel[k]
        out_data = np.zeros((Ss, 2))
        for s in range(1, Ss):
            for r in (1, 2):
                out_data[s-1, r-1] = data[s-1, r-1, kk, :].mean()
        mm = out_data.mean()
        m1 = out_data[:,0].mean()
        m2 = out_data[:,1].mean()
        # if m1 < m2:
        good_r1 = lambda mm: out_data[:,0] < mm
        good_r2 = lambda mm: out_data[:,1] > mm
        # else:
        #     good_r1 = lambda mm: out_data[:,0] > mm
        #     good_r2 = lambda mm: out_data[:,1] < mm
        mm_xx  =  min(good_r1(mm).sum(), good_r2(mm).sum())
        for mm_search in np.linspace(0, 99.99, 10000):
            xx = min(good_r1(mm_search).sum(), good_r2(mm_search).sum())
            if mm_xx < xx:
                xx = mm_xx
                mm = mm_search
        # print(mm, xx)
        good_r1 = good_r1(mm)
        good_r2 = good_r2(mm)
        diff_data = out_data[:,0] - out_data[:,1]
        diff_data_mean = diff_data.mean()
        # diff_data_mean_perc = round((diff_data <= -0.5).sum() / Ss * 100, 2)
        perc1 = round(good_r1.sum() / Ss * 100, 2)
        perc2 = round(good_r2.sum() / Ss * 100, 2)
        perc3 = round(((good_r1 & good_r2).sum()) / Ss * 100, 2)
        perc_both =  min(perc1, perc2)
        # perc2 = round(((out_data[:,0]>mm)&(out_data[:,1]<mm)).sum()/Ss * 100, 2)
        if perc_both > 73:
            print(f"{channel:5s}|{perc_both:10.2f}%|{perc1:10.2f}%|{perc2:10.2f}%|{mm:10.2f}%| {abs(diff_data_mean):10.2f}%")
            # print(set(np.arange(1, Ss+1)[(good_r1 & good_r2)]))

if __name__ == "__main__":
    main()