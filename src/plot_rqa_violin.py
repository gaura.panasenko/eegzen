import matplotlib.pyplot as plt
import numpy as np
import json
import math

from common.params import ALL_CHANNELS, DEMO_CHANNELS, DEMO_S, SAMPLE_RATE, RQA_PARAMS

def main():
    plt.rcParams['figure.constrained_layout.use'] = True
    plt.close('all')

    with open("rqa/rqa.json", "r") as f:
        d = json.load(f)

    for l, (param, descr) in enumerate(RQA_PARAMS.items()):
        t = f"{param}"
        fig, ax = plt.subplots(4, 1, num=t, layout="constrained")
        fig.suptitle(descr)
        for kk, channel in enumerate(DEMO_CHANNELS):
            k = ALL_CHANNELS[channel]
            out_range = np.arange(1, 5)
            out_data = [[] for i in out_range]
            for i in range(1, 110):
                for j in range(1, 3):
                    key_s = f"S{i:03d}R01_{channel}"
                    key = f"S{i:03d}R{j:02d}_{channel}"
                    if key in d and key_s in d:
                        val = d[key][param]
                        if not(math.isnan(val) or math.isinf(val)):
                            out_data[(j-1)].append(val)
                for j in range(0, 2):
                    key_s = f"S{i:03d}R03T0_{channel}"
                    key = f"S{i:03d}R03T{j}_{channel}"
                    if key in d and key_s in d:
                        val = d[key][param]
                        if not(math.isnan(val) or math.isinf(val)):
                            out_data[(j+2)].append(val)
            #ax[k].violinplot(data[l*k])
            if not out_data:
                continue
            for j in out_range:
                print(param, channel, np.mean(out_data[j-1]), min(out_data[j-1]), max(out_data[j-1]))
                if len(out_data[j-1]) == 0:
                    out_data[j-1].append(0)
                zzz = np.array(out_data[j-1])
                zzz_mean = np.mean(zzz)
                # if (zzz > 0.1).sum() / 109 * 100 > 70 or (zzz < -0.1).sum() / 109 * 100 > 70:
                #     print(param, channel, j, (zzz > 0.1).sum() / 109 * 100, (zzz < -0.1).sum() / 109 * 100)
                # print(param, channel, j, ">", (np.array(out_data[j-1]) > 0.5).sum()/109 * 100)
                # print(param, channel, j, "<", (np.array(out_data[j-1]) < -0.5).sum()/109 * 100)
            ax[kk].violinplot(out_data, showmeans=False, showmedians=True)
            fig_labels = [f"State R{j:02d}\n{len(out_data[j-2])} people" for j in range(1,3)]
            fig_labels += [f"State R3T{j}\n{len(out_data[j])} people" for j in range(0,2)]
            ax[kk].set_xticks(out_range, labels=fig_labels)
            ax[kk].set_ylabel(f"{channel}")
            #ax[k].set_xlabel([f"R{j:02d}" for i in range(1,4)])
        # fig.savefig(f"figs/{t.replace('/', '_')}.png")
    plt.show()

if __name__ == "__main__":
    main()