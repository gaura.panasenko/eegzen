import matplotlib.pyplot as plt
import numpy as np
import json
import math
from scipy import signal
from numpy import linalg as LA

from common.params import ALL_CHANNELS, DEMO_CHANNELS, DEMO_S, SAMPLE_RATE, RQA_PARAMS, EPS
from common.rqa_analyse import calc_rp, calc_rqa
from teaspoon.parameter_selection.FNN_n import FNN_n
from teaspoon.parameter_selection.MI_delay import MI_for_delay

def norm(x):
    return np.sqrt(np.mean(x**2)) * 2

def main():
    plt.rcParams['figure.constrained_layout.use'] = True
    plt.rcParams['text.usetex'] = True
    plt.close('all')
    window = int(SAMPLE_RATE * 0.5)
    w = f"{window / SAMPLE_RATE:0.2f}"
    channel = ALL_CHANNELS[DEMO_CHANNELS[2]]
    dt = []

    s = "S001"

    j_names = ["Opened eyes", "Closed eyes"]
    
    for j in range(1, 3):
        file = f"data/{s}R{j:02d}.npy"
        data = np.load(file)
        dt.append(data[:, channel])

    dt_norm = norm(np.concatenate(dt))
    print(dt_norm)

    for j in dt:
        j /= 200
    # data[:, channel] /= LA.norm(data[:, channel])
    # data[:, channel] = signal.detrend(data[:, channel])

    Ls = [[] for _ in dt]

    for j, jd in enumerate(dt):
        fig, ax = plt.subplots(5, 5, sharex=True, sharey=True)

        for i in range(25):
            start = i * window
            x = jd[start:start + window]
            # x /= norm(x)
            ax_i = ax[i//5,i%5]
            tau = MI_for_delay(x)
            m = FNN_n(x, tau)[1]
            calc_rp(x, 1, 4, 0, 1, window, EPS, ax_i)
            rqa = calc_rqa(x, 1, 4, EPS)
            L = rqa['L']
            Ls[j].append(L)
            ax_i.set_title(f"$t_0={start/SAMPLE_RATE:0.2f}, \\tau={tau}, m={m}, L={L:0.2f}$")
        fig.suptitle(f"{s} - $w$={w} - {j_names[j]}")
        fig.set_size_inches(30//2,18//2)
        fig.savefig(f"figs/{s}R{j+1:02d}_w{w}_CRP.png")

    
    fig, ax = plt.subplots(1, 1, sharex=True, sharey=True)
    fig.suptitle(f"{s} - $w$={w} - {RQA_PARAMS['L']}")
    Lx = np.arange(25) * window / SAMPLE_RATE
    for i in range(len(dt)):
        ax.plot(Lx, Ls[i], alpha=0.7, label=f"{j_names[i]}, $L_{{mean}}$={np.mean(Ls[i]):0.2f}")
    ax.set_xlabel("Time, s")
    ax.legend()
    fig.set_size_inches(30//2,18//2)
    fig.savefig(f"figs/{s}_w{w}_L.png")

    # fig, ax = plt.subplots(1, 1, sharex=True, sharey=True)
    # for i in dt:
    #     ax.plot(i, alpha=0.7)
    # fig.set_size_inches(30//2,18//2)
    # fig.savefig(f"figs/{s}_sigs.png")

    plt.show()


if __name__ == "__main__":
    main()