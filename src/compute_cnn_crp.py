import numpy as np
import matplotlib.pyplot as plt
import tensorflow as tf
import cv2
import os

from tensorflow.keras import datasets, layers, models

from common.params import ALL_CHANNELS, DEMO_CHANNELS, DEMO_S, SAMPLE_RATE, RQA_PARAMS

def main():
    plt.rcParams['figure.constrained_layout.use'] = True
    plt.close('all')

    train_data = np.load("cnn_train_data.npy")
    test_data = np.load("cnn_test_data.npy")
    train_labels = np.load("cnn_labels.npy")

    # plt.figure(figsize=(10,10))
    # for i in range(25):
    #     plt.subplot(5,5,i+1)
    #     plt.xticks([])
    #     plt.yticks([])
    #     plt.grid(False)
    #     plt.imshow(train_data[i,:,:,0]*255)
    # plt.show()

    model = models.Sequential()
    model.add(layers.Conv2D(64, (3, 3), activation='relu', input_shape=(64, 64, 4)))
    model.add(layers.MaxPooling2D((2, 2)))
    model.add(layers.Conv2D(128, (3, 3), activation='relu'))
    model.add(layers.MaxPooling2D((2, 2)))
    model.add(layers.Conv2D(128, (3, 3), activation='relu'))
    model.add(layers.Flatten())
    model.add(layers.Dense(128, activation='relu'))
    model.add(layers.Dense(20))
    model.summary()
    model.compile(optimizer='adam',
              loss=tf.keras.losses.SparseCategoricalCrossentropy(from_logits=True),
              metrics=['accuracy'])

    checkpoint_path = "cp.weights.h5"
    checkpoint_dir = os.path.dirname(checkpoint_path)

    cp_callback = tf.keras.callbacks.ModelCheckpoint(filepath=checkpoint_path,
                                                 save_weights_only=True,
                                                 verbose=1)


    history = model.fit(train_data, train_labels, epochs=10, 
                    validation_data=(test_data, train_labels), callbacks=[cp_callback])
    plt.plot(history.history['accuracy'], label='accuracy')
    plt.plot(history.history['val_accuracy'], label='val_accuracy')
    print(history.history['accuracy'][-1] * 100)
    plt.xlabel('Epoch')
    plt.ylabel('Accuracy')
    plt.ylim([0.5, 1])
    plt.legend(loc='lower right')




if __name__ == "__main__":
    main()