import numpy as np
from scipy import signal
from numpy.fft import rfft, rfftfreq

from common.params import ALL_CHANNELS, DEMO_CHANNELS, DEMO_S, SAMPLE_RATE, RQA_PARAMS, EPS
from common import filter_noise_min
from common.process_eeg import bandpass_freq_smooth

nseg = int(SAMPLE_RATE * 5) * 2
f = rfftfreq(nseg, 1 / SAMPLE_RATE)
noise_free = filter_noise_min(f)
alpha_beta = bandpass_freq_smooth(f, 7, 22, 2)

def calc(filename):
    x = np.load(filename)
    nsegs = (x.shape[0] - nseg) // (nseg // 10)
    out = np.zeros((64, nsegs,))

    # for c, channel in enumerate(DEMO_CHANNELS):
    #     e = ALL_CHANNELS[channel]
    for e in range(64):
        c = e

        for i in range(nsegs):
            x_i = x[i*nseg//10:i*nseg//10+nseg, e]
            Zxx = rfft(x_i) * noise_free
            Zxx_alpha_beta = Zxx * alpha_beta
            div = np.abs(Zxx).sum(axis=0)
            alpha_val = np.abs(Zxx_alpha_beta).sum(axis=0) / div * 100
            out[c, i] = alpha_val
    return out


def main():
    if input("Are you sure? (y/n)") != "y":
        return
    data = np.zeros((109, 2, 64, 50))
    for i in range(1, 110):
        i_name = f"S{i:03d}"
        # for j in range(3, 5):
        #     j_name = f"R{j:02d}"
        #     for k in range(3):
        #         name = f"{i_name}{j_name}T{k}"
        #         print(f"Processing {name}...")
        #         filename = f"data/T/{name}.npy"
        #         calc(name, filename, data)
        for j in range(1, 3):
            j_name = f"R{j:02d}"
            name = f"{i_name}{j_name}"
            print(f"Processing {name}...")
            filename = f"data/{name}.npy"
            data[i-1, j-1] = calc(filename)[:,:50]
    np.save("alphas.npy", data)


if __name__ == "__main__":
    main()